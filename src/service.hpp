/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SERVICE_HPP__
#define __SERVICE_HPP__

#include <memory>
#include <map>
#include <iostream>

namespace Plasium
{
enum class Input_Driver;


class Service
{
public :
  static Service& instance();
  ~Service();

  void info(const std::string& message);
  void warning(const std::string& message);
  void error(const std::string& message);

  std::ostream& info();
  std::ostream& warning();
  std::ostream& error();

  Input_Driver get_input_driver();

private :
  Service();

private :
  static std::unique_ptr<Service> s_instance;

  std::map<std::string, std::string> m_parameters;
  int m_debug_level;
  std::string m_prefix_msg;
  std::unique_ptr<std::ostream> m_empty_ostream;
  std::unique_ptr<std::ostream> m_cout_ostream;
  std::unique_ptr<std::ostream> m_cerr_ostream;
};

} /* namespace Plasium */

#endif /* __SERVICE_HPP__ */
