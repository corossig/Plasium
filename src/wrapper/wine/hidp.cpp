/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/hidp.hpp>

#include <service.hpp>
#include <device.hpp>

#include <cassert>

namespace Plasium
{

enum class HidP_Return
{
  Success = 0x00110000
};

std::map<std::string, Function_Hook> HidP::s_hooks;

HidP_Return WINAPI
HidP::GetCaps(Device** device, HIDP_CAPS* capabilities)
{
  Service::instance().info("HidP::GetCaps");
  Service::instance().info() << (*device)->get_name() << " - " << capabilities << std::endl;

  capabilities->Usage = 1;
  capabilities->UsagePage = 7;
  capabilities->InputReportByteLength = 256;
  capabilities->OutputReportByteLength = 0;
  capabilities->FeatureReportByteLength = 0;
  //capabilities->Reserved[17];
  capabilities->NumberLinkCollectionNodes = 0;
  capabilities->NumberInputButtonCaps = 1; // Buttons
  capabilities->NumberInputValueCaps = 6;   // Axis
  capabilities->NumberInputDataIndices = 0;
  capabilities->NumberOutputButtonCaps = 0;
  capabilities->NumberOutputValueCaps = 0;
  capabilities->NumberOutputDataIndices = 0;
  capabilities->NumberFeatureButtonCaps = 0;
  capabilities->NumberFeatureValueCaps = 0;
  capabilities->NumberFeatureDataIndices = 0;

  return HidP_Return::Success;
}


HidP_Return WINAPI
HidP::GetButtonCaps(HIDP_REPORT_TYPE ReportType, HIDP_BUTTON_CAPS* ButtonCaps,
                    uint16_t* ButtonCapsLength, Device* device)
{
  Service::instance().info("HidP::GetButtonCaps");
  assert(ReportType == HidP_Input);

  for (uint16_t i = 0; i < *ButtonCapsLength; ++i)
  {
    ButtonCaps[i].UsagePage = 7;
    ButtonCaps[i].ReportID = 1;
    ButtonCaps[i].IsAlias = false;
    ButtonCaps[i].BitField = 0;
    ButtonCaps[i].LinkCollection = 0;
    ButtonCaps[i].LinkUsage = 0;
    ButtonCaps[i].LinkUsagePage = 0;
    ButtonCaps[i].IsRange = true;
    ButtonCaps[i].IsStringRange = false;
    ButtonCaps[i].IsDesignatorRange = false;
    ButtonCaps[i].IsAbsolute = true;
    //ButtonCaps[i].Reserved[10];

    ButtonCaps[i].Range.UsageMin = 0;
    ButtonCaps[i].Range.UsageMax = 2;
    ButtonCaps[i].Range.StringMin = 0;
    ButtonCaps[i].Range.StringMax = 0;
    ButtonCaps[i].Range.DesignatorMin = 0;
    ButtonCaps[i].Range.DesignatorMax = 0;
    ButtonCaps[i].Range.DataIndexMin = 0;
    ButtonCaps[i].Range.DataIndexMax = 15;
  }

  return HidP_Return::Success;
}



  /*
  NTSTATUS __stdcall HidP_GetValueCaps(
  _In_    HIDP_REPORT_TYPE     ReportType,
  _Out_   PHIDP_VALUE_CAPS     ValueCaps,
  _Inout_ Puint16_t              ValueCapsLength,
  _In_    PHIDP_PREPARSED_DATA PreparsedData
);
  */

/*
  HidP::GetUsages(
                        HidP_Input, pButtonCaps->UsagePage, 0, usage, &usageLength, pPreparsedData,
                        (PCHAR)pRawInput->data.hid.bRawData, pRawInput->data.hid.dwSizeHid
                        ) == HIDP_STATUS_SUCCESS );*/

} /* namespace Plasium */
