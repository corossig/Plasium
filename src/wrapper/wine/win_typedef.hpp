/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __WIN_TYPEDEF_HPP__
#define __WIN_TYPEDEF_HPP__

#include <cstdint>

#if __x86_64__
#define WINAPI
#else
#define WINAPI __attribute__((__stdcall__))
#endif

using WCHAR = char16_t;
using DWORD = uint32_t;
using WORD  = uint16_t;
using BYTE  = uint8_t;

struct GUID {
  uint32_t Data1;
  uint16_t Data2;
  uint16_t Data3;
  uint8_t  Data4[8];
};
inline bool operator==(GUID lhs, GUID rhs)
{
  return
    lhs.Data1    == rhs.Data1 &&
    lhs.Data2    == rhs.Data2 &&
    lhs.Data3    == rhs.Data3 &&
    lhs.Data4[0] == rhs.Data4[0] &&
    lhs.Data4[1] == rhs.Data4[1] &&
    lhs.Data4[2] == rhs.Data4[2] &&
    lhs.Data4[3] == rhs.Data4[3] &&
    lhs.Data4[4] == rhs.Data4[4] &&
    lhs.Data4[5] == rhs.Data4[5] &&
    lhs.Data4[6] == rhs.Data4[6] &&
    lhs.Data4[7] == rhs.Data4[7];
}

typedef GUID* REFGUID;
typedef GUID IID;
typedef IID* REFIID;

enum class Error_Code
{
  Success = 0,
  Device_Not_Connected = 0x48f
};

inline uint32_t to_int(Error_Code e) { return static_cast<uint32_t>(e); }

#endif /* __WIN_TYPEDEF_HPP__ */
