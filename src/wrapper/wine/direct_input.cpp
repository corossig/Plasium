/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/direct_input.hpp>

#include <wrapper/wine/dotnet_device.hpp>
#include <service.hpp>

namespace Plasium
{

enum Result
{
  DI_OK,
  DIERR_BETADIRECTINPUTVERSION
};

std::map<std::string, Plasium::Function_Hook> DInput::s_hooks;

int32_t WINAPI
DInput::DirectInput8Create(uintptr_t hinst, int32_t dwVersion, REFIID riidltf, void** ppvOut, void* punkOuter)
{
  (void) hinst;
  Service::instance().info() << "LibGameapd DirectInput8Create " << dwVersion << std::endl;

  auto& fun_hook = s_hooks.find("DirectInput8Create")->second;
  fun_hook.disable();

  auto orignal_fun = reinterpret_cast<decltype(&DirectInput8Create)>(fun_hook.get_original());
  auto result = orignal_fun(hinst, dwVersion, riidltf, ppvOut, punkOuter);

  fun_hook.enable();

  if (dwVersion < 0x0400)
    return Result::DIERR_BETADIRECTINPUTVERSION;

  if (dwVersion > 0x0800)
    return Result::DIERR_BETADIRECTINPUTVERSION;

  DotNet::s_hook_IDirectInput((uintptr_t**)ppvOut);

  return result;
}

#if 0

/*
Parameters

dwVersion
    Version number of DirectInput for which the application is designed. This value is normally DIRECTINPUT_VERSION. If the application defines DIRECTINPUT_VERSION before including Dinput.h, the value must be greater than 0x0800. For earlier versions, use DirectInputCreateEx, which is in Dinput.lib.
riidltf
    Unique identifier of the desired interface. This value is IID_IDirectInput8A or IID_IDirectInput8W. Passing the IID_IDirectInput8 define selects the ANSI or Unicode version of the interface, depending on whether UNICODE is defined during compilation.
ppvOut
    Address of a pointer to a variable to receive the interface pointer if the call succeeds.
punkOuter
    Pointer to the address of the controlling object's IUnknown interface for COM aggregation, or NULL if the interface is not aggregated. Most calling applications pass NULL. If aggregation is requested, the object returned in ppvOut is a pointer to IUnknown, as required by COM aggregation.

Return Value

If the function succeeds, the return value is DI_OK. If the function fails, the return value can be one of the following error values: DIERR_BETADIRECTINPUTVERSION, DIERR_INVALIDPARAM, DIERR_OLDDIRECTINPUTVERSION, DIERR_OUTOFMEMORY.
*/


HRESULT ConfigureDevices(
         LPDICONFIGUREDEVICESCALLBACK lpdiCallback,
         LPDICONFIGUREDEVICESPARAMS lpdiCDParams,
         DWORD dwFlags,
         LPVOID pvRefData
)
/*
Parameters

lpdiCallback
    Address of a callback function to be called each time the contents of the surface change. See DIConfigureDevicesCallback. Pass NULL if the application does not handle the display of the property sheet. In this case, DirectInput displays the property sheet and returns control to the application when the user closes the property sheet. If you supply a callback pointer, you must also supply a valid surface pointer in the lpUnkDDSTarget member of the DICONFIGUREDEVICESPARAMS structure.
lpdiCDParams
    Address of a DICONFIGUREDEVICESPARAMS structure that contains information about users and genres for the game, as well as information about how the user interface is displayed.
dwFlags
    DWORD value that specifies the mode in which the control panel should be invoked. The dwFlags parameter must be one of the following values.

    DICD_DEFAULT
        Open the property sheet in view-only mode.
    DICD_EDIT
        Open the property sheet in edit mode. This mode enables the user to change action-to-control mappings. After the call returns, the application should assume current devices are no longer valid, release all device interfaces, and reinitialize them by calling IDirectInput8::EnumDevicesBySemantics.

pvRefData
    Application-defined value to pass to the callback function.

Return Value

If the method succeeds, the return value is DI_OK. If the method fails, the return value can be one of the following: DIERR_INVALIDPARAM, DIERR_OUTOFMEMORY.
Remarks

IDirectInput8::ConfigureDevices is deprecated. This method always fails on Windows Vista and later versions of Windows.

Hardware vendors provide bitmaps and other display information for their device.

Before calling the method, an application can modify the text labels associated with each action by changing the value in the lptszActionName member of the DIACTION structure.

Configuration is stored for each user of each device for each game. The information can be retrieved by the IDirectInputDevice8::BuildActionMap method.

By default, acceleration is supported for these pixel formats:

    A1R5G5B5

    16-bit pixel format with 5 bits reserved for each color and 1 bit reserved for alpha (transparent texel).
    A8R8G8B8

    32-bit ARGB pixel format with alpha.
    R9G8B8

    24-bit RGB pixel format.
    X1R5G5B5

    16-bit pixel format with 5 bits reserved for each color.
    X8R8G8B8

    32-bit RGB pixel format with 8 bits reserved for each color.

Other formats will result in color conversion and dramatically slow the frame rate.
Ee417801.note(en-us,VS.85).gifNote
Even if the cooperative level for the application is disabling the Windows logo key passively through an exclusive cooperative level or actively through use of the DISCL_NOWINKEY flag, that key will be active while the default action mapping user interface (UI) is displayed.
*/


HRESULT CreateDevice(
         REFGUID rguid,
         LPDIRECTINPUTDEVICE * lplpDirectInputDevice,
         LPUNKNOWN pUnkOuter
)
/*
Parameters

rguid
    Reference to the GUID for the desired input device (see Remarks). The GUID is retrieved through the IDirectInput8::EnumDevices method, or it can be one of the predefined GUIDs listed below. For the following GUID values to be valid, your application must define INITGUID before all other preprocessor directives at the beginning of the source file, or link to Dxguid.lib.

    GUID_SysKeyboard
        The default system keyboard.
    GUID_SysMouse
        The default system mouse.

lplpDirectInputDevice
    Address of a variable to receive the IDirectInputDevice8 Interface interface pointer if successful.
pUnkOuter
    Address of the controlling object's IUnknown interface for COM aggregation, or NULL if the interface is not aggregated. Most calling applications pass NULL.

Return Value

If the method succeeds, the return value is DI_OK. If the method fails, the return value can be one of the following: DIERR_DEVICENOTREG, DIERR_INVALIDPARAM, DIERR_NOINTERFACE, DIERR_NOTINITIALIZED, DIERR_OUTOFMEMORY.
Remarks

Calling this method with pUnkOuter = NULL is equivalent to creating the object by CoCreateInstance (&CLSID_DirectInputDevice, NULL, CLSCTX_INPROC_SERVER, riid, lplpDirectInputDevice) and then initializing it with Initialize.

Calling this method with pUnkOuter != NULL is equivalent to creating the object by CoCreateInstance (&CLSID_DirectInputDevice, punkOuter, CLSCTX_INPROC_SERVER, &IID_IUnknown, lplpDirectInputDevice). The aggregated object must be initialized manually.
Requirements
*/


typedef struct DIDEVICEINSTANCE {
    DWORD dwSize;
    GUID guidInstance;
    GUID guidProduct;
    DWORD dwDevType;
    TCHAR tszInstanceName[MAX_PATH];
    TCHAR tszProductName[MAX_PATH];
    GUID guidFFDriver;
    WORD wUsagePage;
    WORD wUsage;
} DIDEVICEINSTANCE, *LPDIDEVICEINSTANCE;


DIDEVICEINSTANCE
IDirectInput8::make_device_instance(const Plasium::Device& input_dev)
{
   DIDEVICEINSTANCE device;

   device.dwSize       = sizeof(DIDEVICEINSTANCE);
   device.guidInstance = Plasium::generate_local_guid(input_dev.get_guid());
   device.guidProduct  = input_dev.get_guid();
   device.dwDevType    = 0/*DI8DEVTYPEGAMEPAD_STANDARD ???*/;
   ::memcpy(device.tszInstanceName,
            input_dev.local_name().c_str(),
            std::min(MAX_PATH, input_dev.local_name().size())*sizeof(char));
   ::memcpy(device.tszProductName,
            input_dev.name().c_str(),
            std::min(MAX_PATH, input_dev.name().size())*sizeof(char));
   device.guidFFDriver = input_dev.get_guid();;
   device.wUsagePage;
   device.wUsage;
}


HRESULT
IDirectInput8::EnumDevices(DWORD dwDevType, LPIENUMDEVICESCALLBACK lpCallback, LPVOID pvRef, DWORD dwFlags)
{
   switch (dwDevType)
   {
      case DI8DEVCLASS_ALL :
         for (const auto& input_dev : m_instance.list_input())
         {
            DIDEVICEINSTANCE device = IDirectInput8::make_device_instance(input_dev);
            lpCallback(device, pvRef);
         }
         break;
      case DI8DEVCLASS_DEVICE :
         break;
      case DI8DEVCLASS_GAMECTRL :
         for (const auto& gamepad : m_instance.list_gamepad())
         {
            DIDEVICEINSTANCE device = IDirectInput8::make_device_instance(gamepad);
            lpCallback(device, pvRef);
         }
         break
      case DI8DEVCLASS_KEYBOARD :
         for (const auto& keyboard : m_instance.list_keyboard())
         {
            DIDEVICEINSTANCE device = IDirectInput8::make_device_instance(keyboard);
            lpCallback(device, pvRef);
         }
         break;
      case DI8DEVCLASS_POINTER :
         for (const auto& pointer : m_instance.list_pointer())
         {
            DIDEVICEINSTANCE device = IDirectInput8::make_device_instance(pointer);
            lpCallback(device, pvRef);
         }
         break;
   };

}

/*
Parameters

dwDevType
    Device type filter.

    To restrict the enumeration to a particular type of device, set this parameter to a DI8DEVTYPE_* value. See DIDEVICEINSTANCE.

    To enumerate a class of devices, use one of the following values.

    DI8DEVCLASS_ALL
        All devices.
    DI8DEVCLASS_DEVICE
        All devices that do not fall into another class.
    DI8DEVCLASS_GAMECTRL
        All game controllers.
    DI8DEVCLASS_KEYBOARD
        All keyboards. Equivalent to DI8DEVTYPE_KEYBOARD.
    DI8DEVCLASS_POINTER
        All devices of type DI8DEVTYPE_MOUSE and DI8DEVTYPE_SCREENPOINTER.

lpCallback
    Address of a callback function to be called once for each device enumerated. See DIEnumDevicesCallback.
pvRef
    Application-defined 32-bit value to be passed to the enumeration callback each time it is called.
dwFlags
    Flag value that specifies the scope of the enumeration. This parameter can be one or more of the following values:

    DIEDFL_ALLDEVICES
        All installed devices are enumerated. This is the default behavior.
    DIEDFL_ATTACHEDONLY
        Only attached and installed devices.
    DIEDFL_FORCEFEEDBACK
        Only devices that support force feedback.
    DIEDFL_INCLUDEALIASES
        Include devices that are aliases for other devices.
    DIEDFL_INCLUDEHIDDEN
        Include hidden devices. For more information about hidden devices, see DIDEVCAPS.
    DIEDFL_INCLUDEPHANTOMS
        Include phantom (placeholder) devices.

Return Value

If the method succeeds, the return value is DI_OK. If the method fails, the return value can be one of the following error values: DIERR_INVALIDPARAM, DIERR_NOTINITIALIZED.
Remarks

All installed devices can be enumerated, even if they are not present. For example, a flight stick might be installed on the system but not currently plugged into the computer. Set the dwFlags parameter to indicate whether only attached or all installed devices should be enumerated. If the DIEDFL_ATTACHEDONLY flag is not present, all installed devices are enumerated.

A preferred device type can be passed as a dwDevType filter so that only the devices of that type are enumerated.

On Microsoft Windows XP, DirectInput enumerates only one mouse and one keyboard device, referred to as the system mouse and the system keyboard. These devices represent the combined output of all mice and keyboards respectively on a system. For information about how to read from multiple mice or keyboards individually on Windows XP, see the WM_INPUT documentation.
Ee417804.note(en-us,VS.85).gif*/





HRESULT EnumDevicesBySemantics(
         LPCTSTR ptszUserName,
         LPDIACTIONFORMAT lpdiActionFormat,
         LPDIENUMDEVICESBYSEMANTICSCB lpCallback,
         LPVOID pvRef,
         DWORD dwFlags
)
/*

Parameters

ptszUserName
    String identifying the current user, or NULL to specify the user logged onto the system. The user name is taken into account when enumerating devices. A device with user mappings is preferred to a device without any user mappings. By default, devices in use by other users are not enumerated for this user.
lpdiActionFormat
    Address of a DIACTIONFORMAT structure that specifies the action map for which suitable devices are enumerated.
lpCallback
    Address of a callback function to be called once for each device enumerated. See DIEnumDevicesBySemanticsCallback.
pvRef
    Application-defined 32-bit value to pass to the enumeration callback each time it is called.
dwFlags
    Flag value that specifies the scope of the enumeration. This parameter can be one or more of the following values.

    DIEDBSFL_ATTACHEDONLY
        Only attached and installed devices are enumerated.
    DIEDBSFL_AVAILABLEDEVICES
        Only unowned, installed devices are enumerated.
    DIEDBSFL_FORCEFEEDBACK
        Only devices that support force feedback are enumerated.
    DIEDBSFL_MULTIMICEKEYBOARDS
        Only secondary (non-system) keyboard and mouse devices.
    DIEDBSFL_NONGAMINGDEVICES
        Only HID-compliant devices whose primary purpose is not as a gaming device. Devices such as USB speakers and multimedia buttons on some keyboards would fall within this value.
    DIEDBSFL_THISUSER
        All installed devices for the user identified by ptszUserName, and all unowned devices, are enumerated.
    DIEDBSFL_VALID
        DIEDBSFL_VALID is also defined in Dinput.h, but is not used by applications.

Return Value

If the method succeeds, the return value is DI_OK. If the method fails, the return value can be one of the following error values: DIERR_INVALIDPARAM, DIERR_NOTINITIALIZED.
Remarks

The keyboard and mouse are enumerated last.
    */




HRESULT FindDevice(
         REFGUID rguidClass,
         LPCTSTR ptszName,
         LPGUID pguidInstance
)
/*
Parameters

rguidClass
    Unique identifier of the device class for the device that the application is to locate. The application obtains the class GUID from the device arrival notification. For more information, see the documentation on the DBT_DEVICEARRIVAL event in the Microsoft Platform Software Development Kit (SDK).
ptszName
    Name of the device. The application obtains the name from the device arrival notification.
pguidInstance
    Address of a variable to receive the instance GUID for the device, if the device is found. This value can be passed to IDirectInput8::CreateDevice.

Return Value

If the method succeeds, the return value is DI_OK. If the method fails, the return value can be DIERR_DEVICENOTREG. Failure results if the GUID and name do not correspond to a device class that is registered with DirectInput. For example, they might refer to a storage device, rather than an input device.
*/


HRESULT GetDeviceStatus(
         REFGUID rguidInstance
)
/*
Parameters

rguidInstance
    Reference to (C++) or address of (C) the globally unique identifier (GUID) identifying the instance of the device whose status is being checked.

Return Value

If the method succeeds, the return value is DI_OK if the device is attached to the system, or DI_NOTATTACHED otherwise. If the method fails, the return value can be one of the following error values: DIERR_GENERIC, DIERR_INVALIDPARAM, DIERR_NOTINITIALIZED.
*/




/*
Parameters

hinst
    Instance handle to the application or dynamic-link library (DLL) that is creating the DirectInput object. DirectInput uses this value to determine whether the application or DLL has been certified and to establish any special behaviors that might be necessary for backward compatibility. It is an error for a DLL to pass the handle of the parent application. For example, a Microsoft ActiveX control embedded in a Web page that uses DirectInput must pass its own instance handle, and not the handle of the browser. This ensures that DirectInput recognizes the control and can enable any special behaviors that might be necessary.
dwVersion
    Version number of DirectInput for which the application is designed. This value is normally DIRECTINPUT_VERSION. Passing the version number of a previous version causes DirectInput to emulate that version.

Return Value

If the method succeeds, the return value is DI_OK. If the method fails, the return value can be one of the following error values: DIERR_BETADIRECTINPUTVERSION, DIERR_OLDDIRECTINPUTVERSION.
*/


HRESULT
IDirectInput8::RunControlPanel(HWND hwndOwner, DWORD dwFlags)
{
   // TODO : run GUI of Plasium ( system("plasium_ui") ? )

   return Result::DI_OK;
}


/*
Parameters

hwndOwner
    Handle of the window to be used as the parent window for the subsequent user interface. If this parameter is NULL, no parent window is used.
dwFlags
    Currently not used and must be set to 0.

Return Value

If the method succeeds, the return value is DI_OK. If the method fails, the return value can be one of the following error values: DIERR_INVALIDPARAM, DIERR_NOTINITIALIZED.
*/

#endif



int32_t WINAPI
IDirectInput8::Initialize(uintptr_t self, uintptr_t hinst, int32_t dwVersion)
{
  (void) self; (void)hinst; /* Ignore this parameter, this is used for certification and special behaviors */

  Service::instance().info() << "Call Initialize " << dwVersion << std::endl;

  if (dwVersion < 4)
    return Result::DIERR_BETADIRECTINPUTVERSION;

  if (dwVersion > 8)
    return Result::DIERR_BETADIRECTINPUTVERSION;

  return Result::DI_OK;
}



} /* namespace Plasium */
