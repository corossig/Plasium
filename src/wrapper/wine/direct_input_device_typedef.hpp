/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __WRAPPER_WINE_DIRECT_INPUT_DEVICE_TYPEDEF_HPP__
#define __WRAPPER_WINE_DIRECT_INPUT_DEVICE_TYPEDEF_HPP__

#include <wrapper/wine/win_typedef.hpp>



namespace Plasium
{

namespace DI
{
constexpr int MAX_PATH=260;


template<typename T>
T operator|(T lhs, T rhs)
{
  return static_cast<T>(static_cast<int>(lhs) | static_cast<int>(rhs));
}

template<typename T>
T operator&(T lhs, T rhs)
{
  return static_cast<T>(static_cast<int>(lhs) & static_cast<int>(rhs));
}

template<typename T>
T operator+(T lhs, T rhs)
{
  return static_cast<T>(static_cast<int>(lhs) + static_cast<int>(rhs));
}



enum class Return : uint32_t
{
  Ok = 0,
  InvalidParam = 0x80070057
};


enum class DC : uint32_t
{
  Attached         = 0x00000001,
  Poll_Device      = 0x00000002,
  Emulated         = 0x00000004,
  PolledDataFormat = 0x00000008,
  ForceFeddBack    = 0x00000100,
  FFAttack         = 0x00000200,
  FFFade           = 0x00000400,
  Saturation       = 0x00000800,
  PosNegCoefficients = 0x00001000,
  PosNegSaturation = 0x00002000,
  Deadband         = 0x00004000,
  StartDelay       = 0x00008000,
  Alias            = 0x00010000,
  Phantom          = 0x00020000
};
//template<> DevCaps operator|(DevCaps lhs, DevCaps rhs);

enum class DevType : uint32_t
{
  Device   = 1,
  Mouse    = 2,
  Keyboard = 3,
  Joystick = 4,
  HID      = 0x10000,


  MOUSE_UNKNOWN         = 1 << 8,
  MOUSE_TRADITIONAL     = 2 << 8,
  MOUSE_FINGERSTICK     = 3 << 8,
  MOUSE_TOUCHPAD        = 4 << 8,
  MOUSE_TRACKBALL       = 5 << 8,


  KEYBOARD_UNKNOWN      = 0 << 8,
  KEYBOARD_PCXT         = 1 << 8,
  KEYBOARD_OLIVETTI     = 2 << 8,
  KEYBOARD_PCAT         = 3 << 8,
  KEYBOARD_PCENH        = 4 << 8,
  KEYBOARD_NOKIA1050    = 5 << 8,
  KEYBOARD_NOKIA9140    = 6 << 8,
  KEYBOARD_NEC98        = 7 << 8,
  KEYBOARD_NEC98LAPTOP  = 8 << 8,
  KEYBOARD_NEC98106     = 9 << 8,
  KEYBOARD_JAPAN106     = 10 << 8,
  KEYBOARD_JAPANAX      = 11 << 8,
  KEYBOARD_J3100        = 12 << 8,


  JOYSTICK_UNKNOWN      = 1 << 8,
  JOYSTICK_TRADITIONAL  = 2 << 8,
  JOYSTICK_FLIGHTSTICK  = 3 << 8,
  JOYSTICK_GAMEPAD      = 4 << 8,
  JOYSTICK_RUDDER       = 5 << 8,
  JOYSTICK_WHEEL        = 6 << 8,
  JOYSTICK_HEADTRACKER  = 7 << 8
};

enum class DI8DevType : uint32_t
{
  Device           = 0x11,
  Mouse            = 0x12,
  Keyboard         = 0x13,
  Joystick         = 0x14,
  Gamepad          = 0x15,
  DRIVING          = 0x16,
  FLIGHT           = 0x17,
  _1STPERSON       = 0x18,
  DEVICECTRL       = 0x19,
  SCREENPOINTER    = 0x1A,
  REMOTE           = 0x1B,
  SUPPLEMENTAL     = 0x1C,

  MOUSE_UNKNOWN                   = 1 << 8,
  MOUSE_TRADITIONAL               = 2 << 8,
  MOUSE_FINGERSTICK               = 3 << 8,
  MOUSE_TOUCHPAD                  = 4 << 8,
  MOUSE_TRACKBALL                 = 5 << 8,
  MOUSE_ABSOLUTE                  = 6 << 8,

  KEYBOARD_UNKNOWN                = 0 << 8,
  KEYBOARD_PCXT                   = 1 << 8,
  KEYBOARD_OLIVETTI               = 2 << 8,
  KEYBOARD_PCAT                   = 3 << 8,
  KEYBOARD_PCENH                  = 4 << 8,
  KEYBOARD_NOKIA1050              = 5 << 8,
  KEYBOARD_NOKIA9140              = 6 << 8,
  KEYBOARD_NEC98                  = 7 << 8,
  KEYBOARD_NEC98LAPTOP            = 8 << 8,
  KEYBOARD_NEC98106               = 9 << 8,
  KEYBOARD_JAPAN106               = 10 << 8,
  KEYBOARD_JAPANAX                = 11 << 8,
  KEYBOARD_J3100                  = 12 << 8,

  LIMITEDGAMESUBTYPE              = 1 << 8,

  JOYSTICK_LIMITED                = LIMITEDGAMESUBTYPE,
  JOYSTICK_STANDARD               = 2 << 8,

  GAMEPAD_LIMITED                 = LIMITEDGAMESUBTYPE,
  GAMEPAD_STANDARD                = 2 << 8,
  GAMEPAD_TILT                    = 3 << 8,

  DRIVING_LIMITED                 = LIMITEDGAMESUBTYPE,
  DRIVING_COMBINEDPEDALS          = 2 << 8,
  DRIVING_DUALPEDALS              = 3 << 8,
  DRIVING_THREEPEDALS             = 4 << 8,
  DRIVING_HANDHELD                = 5 << 8,

  FLIGHT_LIMITED                  = LIMITEDGAMESUBTYPE,
  FLIGHT_STICK                    = 2 << 8,
  FLIGHT_YOKE                     = 3 << 8,
  FLIGHT_RC                       = 4 << 8,

  _1STPERSON_LIMITED              = LIMITEDGAMESUBTYPE,
  _1STPERSON_UNKNOWN              = 2 << 8,
  _1STPERSON_SIXDOF               = 3 << 8,
  _1STPERSON_SHOOTER              = 4 << 8,

  SCREENPTR_UNKNOWN               = 2 << 8,
  SCREENPTR_LIGHTGUN              = 3 << 8,
  SCREENPTR_LIGHTPEN              = 4 << 8,
  SCREENPTR_TOUCH                 = 5 << 8,

  REMOTE_UNKNOWN                  = 2 << 8,

  DEVICECTRL_UNKNOWN              = 2 << 8,
  DEVICECTRL_COMMSSELECTION       = 3 << 8,
  DEVICECTRL_COMMSSELECTION_HARDWIRED = 4 << 8,

  SUPPLEMENTAL_UNKNOWN            = 2 << 8,
  SUPPLEMENTAL_2NDHANDCONTROLLER  = 3 << 8,
  SUPPLEMENTAL_HEADTRACKER        = 4 << 8,
  SUPPLEMENTAL_HANDTRACKER        = 5 << 8,
  SUPPLEMENTAL_SHIFTSTICKGATE     = 6 << 8,
  SUPPLEMENTAL_SHIFTER            = 7 << 8,
  SUPPLEMENTAL_THROTTLE           = 8 << 8,
  SUPPLEMENTAL_SPLITTHROTTLE      = 9 << 8,
  SUPPLEMENTAL_COMBINEDPEDALS     = 10 << 8,
  SUPPLEMENTAL_DUALPEDALS         = 11 << 8,
  SUPPLEMENTAL_THREEPEDALS        = 12 << 8,
  SUPPLEMENTAL_RUDDERPEDALS       = 13 << 8
};


struct JoyState
{
  uint32_t lX;             /* x-axis position              */
  uint32_t lY;             /* y-axis position              */
  uint32_t lZ;             /* z-axis position              */
  uint32_t lRx;            /* x-axis rotation              */
  uint32_t lRy;            /* y-axis rotation              */
  uint32_t lRz;            /* z-axis rotation              */
  uint32_t rglSlider[2];   /* extra axes positions         */
  int32_t  rgdwPOV[4];     /* POV directions               */
  uint8_t  rgbButtons[32]; /* 32 buttons                   */
};


enum class JoyState_Offset : uint32_t
{
  X    = 0x000,
  Y    = 0x004,
  Z    = 0x008,
  RX   = 0x00c,
  Ry   = 0x010,
  Rz   = 0x014,

  Slider_0 = 0x018,
  Slider_1 = 0x01c,

  POV_0 = 0x20,
  POV_1 = 0x24,
  POV_2 = 0x28,
  POV_3 = 0x2c,

  Button_0  = 0x30,
  Button_1  = 0x31,
  Button_2  = 0x32,
  Button_3  = 0x33,
  Button_4  = 0x34,
  Button_5  = 0x35,
  Button_6  = 0x36,
  Button_7  = 0x37,
  Button_8  = 0x38,
  Button_9  = 0x39,
  Button_10 = 0x3a,
  Button_11 = 0x3b,
  Button_12 = 0x3c,
  Button_13 = 0x3d,
  Button_14 = 0x3e,
  Button_15 = 0x3f,
  Button_16 = 0x40,
  Button_17 = 0x41,
  Button_18 = 0x42,
  Button_19 = 0x43,
  Button_20 = 0x44,
  Button_21 = 0x45,
  Button_22 = 0x46,
  Button_23 = 0x47,
  Button_24 = 0x48,
  Button_25 = 0x49,
  Button_26 = 0x4a,
  Button_27 = 0x4b,
  Button_28 = 0x4c,
  Button_29 = 0x4d,
  Button_30 = 0x4e,
  Button_31 = 0x4f
};

enum class ObjectType : uint32_t
{
  All = 0x00000000,

  RelAxis = 0x00000001,
  AbsAxis = 0x00000002,
  Axis    = 0x00000003,

  PushButton   = 0x00000004,
  ToggleButton = 0x00000008,
  Button       = 0x0000000C,

  POV        = 0x00000010,
  Collection = 0x00000040,
  NoData     = 0x00000080,

  AnyInstance  = 0x00FFFF00,
  InstanceMask = AnyInstance,

  NoCollection    = 0x00FFFF00,
  FFActuator      = 0x01000000,
  FFEffectTrigger = 0x02000000,

  Output        = 0x10000000,
  VendorDefined = 0x04000000,
  Alias         = 0x08000000
};

inline ObjectType ObjectType_EnumCollection(uint16_t n) { return static_cast<ObjectType>(n << 8); }
inline ObjectType ObjectType_MakeInstance(uint16_t n)   { return static_cast<ObjectType>(n << 8); }

/*#define DIDFT_GETTYPE(n)     LOBYTE(n)
#define DIDFT_GETINSTANCE(n) LOWORD((n) >> 8)
*/

struct DevCaps
{
  uint32_t   dwSize;
  DC         dwFlags;
  DI8DevType dwDevType;
  uint32_t   dwAxes;
  uint32_t   dwButtons;
  uint32_t   dwPOVs;
  uint32_t   dwFFSamplePeriod;
  uint32_t   dwFFMinTimeResolution;
  uint32_t   dwFirmwareRevision;
  uint32_t   dwHardwareRevision;
  uint32_t   dwFFDriverVersion;
};

struct PropHeader
{
  uint32_t dwSize;
  uint32_t dwHeaderSize;
  uint32_t dwObj;
  uint32_t dwHow;
};


struct Envelope
{
  uint32_t dwSize;                   /* sizeof(DIENVELOPE)   */
  uint32_t dwAttackLevel;
  uint32_t dwAttackTime;             /* Microseconds         */
  uint32_t dwFadeLevel;
  uint32_t dwFadeTime;               /* Microseconds         */
};


struct Effect
{
  uint32_t  dwSize;                   /* sizeof(DIEFFECT)     */
  uint32_t  dwFlags;                  /* DIEFF_*              */
  uint32_t  dwDuration;               /* Microseconds         */
  uint32_t  dwSamplePeriod;           /* Microseconds         */
  uint32_t  dwGain;
  uint32_t  dwTriggerButton;          /* or DIEB_NOTRIGGER    */
  uint32_t  dwTriggerRepeatInterval;  /* Microseconds         */
  uint32_t  cAxes;                    /* Number of axes       */
  uint32_t* rgdwAxes;                 /* Array of axes        */
  uint32_t* rglDirection;             /* Array of directions  */
  Envelope* lpEnvelope;               /* Optional             */
  uint32_t  cbTypeSpecificParams;     /* Size of params       */
  void*     lpvTypeSpecificParams;    /* Pointer to params    */
  uint32_t  dwStartDelay;             /* Microseconds         */
};


struct FileEffect
{
  uint32_t      dwSize;
  GUID          GuidEffect;
  const Effect* lpDiEffect;
  char          szFriendlyName[MAX_PATH];
};


struct DeviceObjectData
{
  uint32_t dwOfs;
  uint32_t dwData;
  uint32_t dwTimeStamp;
  uint32_t dwSequence;
};


struct EffectInfoA
{
  uint32_t dwSize;
  GUID     guid;
  uint32_t dwEffType;
  uint32_t dwStaticParams;
  uint32_t dwDynamicParams;
  char     tszName[MAX_PATH];
};


struct EffectInfoW
{
  uint32_t dwSize;
  GUID     guid;
  uint32_t dwEffType;
  uint32_t dwStaticParams;
  uint32_t dwDynamicParams;
  char16_t tszName[MAX_PATH];
};


struct EffEscape
{
  uint32_t dwSize;
  uint32_t dwCommand;
  void*    lpvInBuffer;
  uint32_t cbInBuffer;
  void*    lpvOutBuffer;
  uint32_t cbOutBuffer;
};


struct ObjectDataFormat
{
  const GUID*     pguid;
  JoyState_Offset dwOfs;
  ObjectType      dwType;
  uint32_t        dwFlags;
};

struct DataFormat
{
  uint32_t          dwSize;
  uint32_t          dwObjSize;
  uint32_t          dwFlags;
  uint32_t          dwDataSize;
  uint32_t          dwNumObjs;
  ObjectDataFormat* rgodf;
};


struct DeviceObjectInstanceA
{
  uint32_t dwSize;
  GUID     guidType;
  uint32_t dwOfs;
  uint32_t dwType;
  uint32_t dwFlags;
  char     tszName[MAX_PATH];
  uint32_t dwFFMaxForce;
  uint32_t dwFFForceResolution;
  uint16_t wCollectionNumber;
  uint16_t wDesignatorIndex;
  uint16_t wUsagePage;
  uint16_t wUsage;
  uint32_t dwDimension;
  uint16_t wExponent;
  uint16_t wReportId;
};


struct DeviceInstanceA
{
  uint32_t dwSize;
  GUID     guidInstance;
  GUID     guidProduct;
  DevType  dwDevType;
  char     tszInstanceName[MAX_PATH];
  char     tszProductName[MAX_PATH];
  GUID     guidFFDriver;
  uint16_t wUsagePage;
  uint16_t wUsage;
};

struct DeviceInstanceW
{
  uint32_t dwSize;
  GUID     guidInstance;
  GUID     guidProduct;
  DevType  dwDevType;
  char16_t tszInstanceName[MAX_PATH];
  char16_t tszProductName[MAX_PATH];
  GUID     guidFFDriver;
  uint16_t wUsagePage;
  uint16_t wUsage;
};


typedef struct IDirectInputEffect *LPDIRECTINPUTEFFECT;
typedef bool (/*FAR PASCAL*/ * LPDIENUMCREATEDEFFECTOBJECTSCALLBACK)(LPDIRECTINPUTEFFECT, void*);


typedef bool (/*FAR PASCAL*/ * EnumEffectsA_CB)(const EffectInfoA*, void*);
typedef bool (/*FAR PASCAL*/ * EnumEffects_CB)(const EffectInfoW*, void*);
typedef bool (/*FAR PASCAL*/ * EnumEffectsInFile_CB)(const FileEffect*, void*);
typedef bool (/*FAR PASCAL*/ * EnumDeviceObjectsA_CB)(const DeviceObjectInstanceA*, void*);
typedef bool WINAPI (* EnumDevices_CB)(DeviceInstanceW* dev, void* pvRef);


// Predefined GUIDs
constexpr GUID GUID_XAxis  = {0xA36D02E0,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };
constexpr GUID GUID_YAxis  = {0xA36D02E1,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };
constexpr GUID GUID_ZAxis  = {0xA36D02E2,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };
constexpr GUID GUID_RxAxis = {0xA36D02F4,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };
constexpr GUID GUID_RyAxis = {0xA36D02F5,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };
constexpr GUID GUID_RzAxis = {0xA36D02E3,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };
constexpr GUID GUID_Slider = {0xA36D02E4,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };

constexpr GUID GUID_Button = {0xA36D02F0,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };
constexpr GUID GUID_Key    = {0x55728220,0xD33C,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };

constexpr GUID GUID_POV    = {0xA36D02F2,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };

constexpr GUID GUID_Unknown = {0xA36D02F3,0xC9F3,0x11CF, {0xBF,0xC7,0x44,0x45,0x53,0x54,0x00,0x00} };



} /* namespace DI */


} /* namespace Plasium */

#endif /* __WRAPPER_WINE_DIRECT_INPUT_DEVICE_TYPEDEF_HPP__ */
