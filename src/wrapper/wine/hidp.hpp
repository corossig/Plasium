/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __WRAPPER_WINE_HIDP_HPP__
#define __WRAPPER_WINE_HIDP_HPP__

#include <wrapper/wine/win_typedef.hpp>
#include <wrapper/wine/function_hook.hpp>

#include <map>
#include <string>

typedef struct _HIDP_CAPS {
  uint16_t Usage;
  uint16_t UsagePage;
  uint16_t InputReportByteLength;
  uint16_t OutputReportByteLength;
  uint16_t FeatureReportByteLength;
  uint16_t Reserved[17];
  uint16_t NumberLinkCollectionNodes;
  uint16_t NumberInputButtonCaps;
  uint16_t NumberInputValueCaps;
  uint16_t NumberInputDataIndices;
  uint16_t NumberOutputButtonCaps;
  uint16_t NumberOutputValueCaps;
  uint16_t NumberOutputDataIndices;
  uint16_t NumberFeatureButtonCaps;
  uint16_t NumberFeatureValueCaps;
  uint16_t NumberFeatureDataIndices;
} HIDP_CAPS, *PHIDP_CAPS;


typedef enum _HIDP_REPORT_TYPE {
  HidP_Input,
  HidP_Output,
  HidP_Feature
} HIDP_REPORT_TYPE;


typedef struct _HIDP_BUTTON_CAPS {
  uint16_t   UsagePage;
  uint8_t   ReportID;
  bool IsAlias;
  uint16_t  BitField;
  uint16_t  LinkCollection;
  uint16_t   LinkUsage;
  uint16_t   LinkUsagePage;
  bool IsRange;
  bool IsStringRange;
  bool IsDesignatorRange;
  bool IsAbsolute;
  uint32_t   Reserved[10];
  union {
    struct {
      uint16_t  UsageMin;
      uint16_t  UsageMax;
      uint16_t StringMin;
      uint16_t StringMax;
      uint16_t DesignatorMin;
      uint16_t DesignatorMax;
      uint16_t DataIndexMin;
      uint16_t DataIndexMax;
    } Range;
    struct {
      uint16_t  Usage;
      uint16_t  Reserved1;
      uint16_t StringIndex;
      uint16_t Reserved2;
      uint16_t DesignatorIndex;
      uint16_t Reserved3;
      uint16_t DataIndex;
      uint16_t Reserved4;
    } NotRange;
  };
} HIDP_BUTTON_CAPS, *PHIDP_BUTTON_CAPS;


namespace Plasium
{
enum class HidP_Return;
class Device;

class HidP
{
public :
  static HidP_Return WINAPI GetCaps(Device** device, HIDP_CAPS* Capabilities);

  static HidP_Return WINAPI GetButtonCaps(HIDP_REPORT_TYPE ReportType, HIDP_BUTTON_CAPS* ButtonCaps,
                                          uint16_t* ButtonCapsLength, Device* device);

  static std::map<std::string, Plasium::Function_Hook> s_hooks;
};


} /* namespace Plasium */

#endif /* __WRAPPER_WINE_HIDP_HPP__ */
