/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __WRAPPER_WINE_DIRECT_INPUT_DEVICE_HPP__
#define __WRAPPER_WINE_DIRECT_INPUT_DEVICE_HPP__

#include <wrapper/wine/direct_input_device_typedef.hpp>

#include <vector>
#include <memory>
#include <input/SDL2.hpp>

namespace Plasium
{

class IDirectInputDevice
{
public :
  IDirectInputDevice();
  ~IDirectInputDevice();

  /*** IUnknown methods ***/
  virtual DI::Return WINAPI QueryInterface(REFIID riid, void** ppvObject);
  virtual DI::Return WINAPI AddRef();
  virtual DI::Return WINAPI Release();
  /*** IDirectInputDeviceA methods ***/
  virtual DI::Return WINAPI GetCapabilities(DI::DevCaps* lpDIDevCaps);
  virtual DI::Return WINAPI EnumObjects(DI::EnumDeviceObjectsA_CB lpCallback, void* pvRef, uint32_t dwFlags);
  virtual DI::Return WINAPI GetProperty(REFGUID rguidProp, DI::PropHeader* pdiph);
  virtual DI::Return WINAPI SetProperty(REFGUID rguidProp, const DI::PropHeader* pdiph);
  virtual DI::Return WINAPI Acquire();
  virtual DI::Return WINAPI Unacquire();
  virtual DI::Return WINAPI GetDeviceState(uint32_t cbData, uint8_t* lpvData);
  virtual DI::Return WINAPI GetDeviceData(uint32_t cbObjectData, DI::DeviceObjectData* rgdod, uint32_t* pdwInOut, uint32_t dwFlags);
  virtual DI::Return WINAPI SetDataFormat(const DI::DataFormat* lpdf);
  virtual DI::Return WINAPI SetEventNotification(void* hEvent);
  virtual DI::Return WINAPI SetCooperativeLevel(void* hwnd, uint32_t dwFlags);
  virtual DI::Return WINAPI GetObjectInfo(DI::DeviceObjectInstanceA* pdidoi, uint32_t dwObj, uint32_t dwHow);
  virtual DI::Return WINAPI GetDeviceInfo(DI::DeviceInstanceA* pdidi);
  virtual DI::Return WINAPI RunControlPanel(void* hwndOwner, uint32_t dwFlags);
  virtual DI::Return WINAPI Initialize(void* hinst, uint32_t dwVersion, REFGUID rguid);
  /*** IDirectInputDevice2A methods ***/
  virtual DI::Return WINAPI CreateEffect(REFGUID rguid, const DI::Effect* lpeff, DI::LPDIRECTINPUTEFFECT *ppdeff, void* punkOuter);
  virtual DI::Return WINAPI EnumEffects(DI::EnumEffectsA_CB lpCallback, void* pvRef, uint32_t dwEffType);
  virtual DI::Return WINAPI GetEffectInfo(DI::EffectInfoA* pdei, REFGUID rguid);
  virtual DI::Return WINAPI GetForceFeedbackState(uint32_t* pdwOut);
  virtual DI::Return WINAPI SendForceFeedbackCommand(uint32_t dwFlags);
  virtual DI::Return WINAPI EnumCreatedEffectObjects(DI::LPDIENUMCREATEDEFFECTOBJECTSCALLBACK lpCallback, void* pvRef, uint32_t fl);
  virtual DI::Return WINAPI Escape( DI::EffEscape* pesc);
  virtual DI::Return WINAPI Poll();
  virtual DI::Return WINAPI SendDeviceData(uint32_t cbObjectData, const DI::DeviceObjectData* rgdod, uint32_t pdwInOut, uint32_t fl);
  /*** IDirectInputDevice7A methods ***/
  virtual DI::Return WINAPI EnumEffectsInFile(char* lpszFileName, DI::EnumEffectsInFile_CB pec, void* pvRef, uint32_t dwFlags);
  virtual DI::Return WINAPI WriteEffectToFile(char* lpszFileName, uint32_t dwEntries, DI::FileEffect* rgDiFileEft, uint32_t dwFlags);


private :
  uint32_t m_packet_size;
  std::vector<DI::ObjectDataFormat> m_data_format;
  Device* m_device;

  static std::vector<Device*> s_list_devices;
};

} /* namespace Plasium */

#endif /* __WRAPPER_WINE_DIRECT_INPUT_DEVICE_HPP__ */
