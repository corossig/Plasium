/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __WRAPPER_WINE_DIRECT_INPUT_HPP__
#define __WRAPPER_WINE_DIRECT_INPUT_HPP__

#include <wrapper/wine/win_typedef.hpp>

#include <map>
#include <string>
#include <wrapper/wine/function_hook.hpp>

namespace Plasium
{

class DInput
{
public :
  static int32_t WINAPI DirectInput8Create(uintptr_t hinst, int32_t dwVersion, REFIID riidltf, void** ppvOut, void* punkOuter);

  static std::map<std::string, Plasium::Function_Hook> s_hooks;
};

class IDirectInput8
{
public :
  static int32_t WINAPI Initialize(uintptr_t self, uintptr_t hinst, int32_t dwVersion);

};


} /* namespace Plasium */

#endif /* __WRAPPER_WINE_DIRECT_INPUT_HPP__ */
