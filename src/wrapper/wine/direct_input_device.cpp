/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/direct_input_device.hpp>

#include <assert.h>

#include <mapping.hpp>
#include <service.hpp>
#include <manager.hpp>

namespace Plasium
{

std::vector<Device*> IDirectInputDevice::s_list_devices;

IDirectInputDevice::IDirectInputDevice()
  : m_packet_size(0)
{
  Service::instance().info("IDirectInputDevice::constructor");

  if (s_list_devices.empty())
  {
    s_list_devices = Manager::instance().get_devices();
  }

  m_device = s_list_devices[0];
}


IDirectInputDevice::~IDirectInputDevice()
{
}


DI::Return WINAPI
IDirectInputDevice::QueryInterface(REFIID riid, void** ppvObject)
{
  Service::instance().info("IDirectInputDevice::QueryInterface");
  *ppvObject = this;

  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::AddRef()
{
  Service::instance().info("IDirectInputDevice::AddRef");
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::Release()
{
  Service::instance().info("IDirectInputDevice::Release");
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetCapabilities(DI::DevCaps* lpDIDevCaps)
{
  Service::instance().info("IDirectInputDevice::GetCapabilities");

  assert(lpDIDevCaps->dwSize = sizeof(DI::DevCaps));

  lpDIDevCaps->dwFlags   = DI::DC::Attached | DI::DC::Emulated;
  lpDIDevCaps->dwDevType = DI::DI8DevType::Gamepad + DI::DI8DevType::GAMEPAD_STANDARD;
  lpDIDevCaps->dwAxes    = 6; // Left + Right + L2 + R2
  lpDIDevCaps->dwButtons = 11;
  lpDIDevCaps->dwPOVs    = 2; // up/down + left/right
  lpDIDevCaps->dwFFSamplePeriod      = 0;
  lpDIDevCaps->dwFFMinTimeResolution = 0;
  lpDIDevCaps->dwFirmwareRevision    = 0;
  lpDIDevCaps->dwHardwareRevision    = 0;
  lpDIDevCaps->dwFFDriverVersion     = 0;

  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::EnumObjects(DI::EnumDeviceObjectsA_CB lpCallback, void* pvRef, uint32_t dwFlags)
{
  Service::instance().info("IDirectInputDevice::EnumObjects");

  Mapping* mapping = nullptr;
  const auto& buttons_mapping = mapping->get_buttons_mapping();

  for (size_t i = 0; i < buttons_mapping.size(); ++i)
  {

  }
  // TODO

  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetProperty(REFGUID rguidProp, DI::PropHeader* pdiph)
{
  Service::instance().info("IDirectInputDevice::GetProperty");

  assert(false);
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::SetProperty(REFGUID rguidProp, const DI::PropHeader* pdiph)
{
  Service::instance().info("IDirectInputDevice::SetProperty");
  //assert(false);
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::Acquire()
{
  Service::instance().info("IDirectInputDevice::Acquire");
  // Always Ok
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::Unacquire()
{
  Service::instance().info("IDirectInputDevice::Unacquire");
  // Always Ok
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetDeviceState(uint32_t cbData, uint8_t* lpvData)
{
  Service::instance().info("IDirectInputDevice::GetDeviceState");
  ::memset(lpvData, 0, cbData*sizeof(uint8_t));

  const auto& state = m_device->get_state();
  const auto& buttons_state = m_device->get_raw_buttons_state();

  for (const auto& odf : m_data_format)
  {
    if (static_cast<uint32_t>(odf.dwOfs) >= static_cast<uint32_t>(DI::JoyState_Offset::Button_0) &&
        static_cast<uint32_t>(odf.dwOfs) <= static_cast<uint32_t>(DI::JoyState_Offset::Button_31) )
    {
      auto button_num = static_cast<uint32_t>(odf.dwOfs);
      auto offset = button_num;
      button_num -= static_cast<uint32_t>(DI::JoyState_Offset::Button_0);

      if (button_num < buttons_state.size())
        lpvData[offset] = buttons_state[button_num];
    }

  }
#if 0
      case DI::JoyState::X :
      break;
  case DI::JoyState::Y    = 0x004,
  case DI::JoyState::Z    = 0x008,
  case DI::JoyState::RX   = 0x00c,
  case DI::JoyState::Ry   = 0x010,
  case DI::JoyState::Rz   = 0x014,

  case DI::JoyState::Slider_0 = 0x018,
  case DI::JoyState::Slider_1 = 0x01c,

  case DI::JoyState::POV_0 = 0x20,
  case DI::JoyState::POV_1 = 0x24,
  case DI::JoyState::POV_2 = 0x28,
  case DI::JoyState::POV_3 = 0x2c,
    };
#endif
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetDeviceData(uint32_t cbObjectData, DI::DeviceObjectData* rgdod, uint32_t* pdwInOut, uint32_t dwFlags)
{
  Service::instance().info("IDirectInputDevice::GetDeviceData");
  //assert(false);
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::SetDataFormat(const DI::DataFormat* lpdf)
{
  Service::instance().info("IDirectInputDevice::SetDataFormat");

  assert(lpdf->dwSize    == sizeof(DI::DataFormat));
  assert(lpdf->dwObjSize == sizeof(DI::ObjectDataFormat));

  // Only support absolut axis
  if (lpdf->dwFlags != 0x00000001 /* DIDF_ABSAXIS /!\ This is not DIDFT_ABSAXIS /!\ */)
  {
    Service::instance().error("IDirectInputDevice::SetDataFormat invalid");
    return DI::Return::InvalidParam;
  }

  m_packet_size = lpdf->dwDataSize;
  m_data_format.resize(0);
  m_data_format.reserve(m_packet_size);
  for (uint32_t i = 0; i < lpdf->dwNumObjs; ++i)
  {
    const auto& rgodf = lpdf->rgodf[i];
    m_data_format.emplace_back(rgodf);
  }

  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::SetEventNotification(void* hEvent)
{
  Service::instance().info("IDirectInputDevice::SetEventNotification");
  assert(false);
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::SetCooperativeLevel(void* hwnd, uint32_t dwFlags)
{
  Service::instance().info("IDirectInputDevice::SetCooperativeLevel");
  //assert(false);
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetObjectInfo(DI::DeviceObjectInstanceA* pdidoi, uint32_t dwObj, uint32_t dwHow)
{
  Service::instance().info("IDirectInputDevice::GetObjectInfo");
  // TODO
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetDeviceInfo(DI::DeviceInstanceA* pdidi)
{
  Service::instance().info("IDirectInputDevice::GetDeviceInfo");
  assert(false);
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::RunControlPanel(void* hwndOwner, uint32_t dwFlags)
{
  // TODO : Call system("plasium_qt") or a thing like that
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::Initialize(void* hinst, uint32_t dwVersion, REFGUID rguid)
{
  Service::instance().info("IDirectInputDevice::Initialize");
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::CreateEffect(REFGUID, const DI::Effect*, DI::LPDIRECTINPUTEFFECT *, void*)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::EnumEffects(DI::EnumEffectsA_CB, void*, uint32_t)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetEffectInfo(DI::EffectInfoA*, REFGUID)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::GetForceFeedbackState(uint32_t*)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::SendForceFeedbackCommand(uint32_t)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::EnumCreatedEffectObjects(DI::LPDIENUMCREATEDEFFECTOBJECTSCALLBACK, void*, uint32_t)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::Escape(DI::EffEscape*)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::Poll()
{
  // Do the polling in GetState
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::SendDeviceData(uint32_t, const DI::DeviceObjectData*, uint32_t, uint32_t)
{
  // Ignore device data
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::EnumEffectsInFile(char*, DI::EnumEffectsInFile_CB, void*, uint32_t)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


DI::Return WINAPI
IDirectInputDevice::WriteEffectToFile(char*, uint32_t, DI::FileEffect*, uint32_t)
{
  // Don't support force feedback
  return DI::Return::Ok;
}


} /* namespace Plasium */
