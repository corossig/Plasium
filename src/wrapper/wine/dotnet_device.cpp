/*
 *  All ideas in this file come from https://github.com/KoKuToru/koku-xinput-wine/
 *
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/dotnet_device.hpp>

#include <wrapper/wine/direct_input_device.hpp>
#include <service.hpp>

#include <string>
#include <cstring>
#include <codecvt>
#include <locale>

namespace Plasium
{

/**
 *  Here the plan :
 *  1 - Intercept CoSetProxyBlanket to get a IWbemServices object (Init)
 *  2 - Modify virtual table of IWbemServices to point on our custom Create{Instance,Class}Enum
 *  3 - In Create...Enum, modify virtual table of IEnumWbemClassObject to point on our Next
 *
 **/
DotNet* DotNet::s_dotnet_instance = nullptr;

DotNet::DotNet(void* handle)
  : m_wbem_vtbl(nullptr)
  , m_enum_wbem_vtbl(nullptr)
  , m_dinput8_vtbl(nullptr)
{
  Service::instance().info("DotNet init");

  s_dotnet_instance = this;

  m_CoSetProxyBlanket_hook
    = new Function_Hook(handle,
                        "CoSetProxyBlanket",
                        reinterpret_cast<intptr_t>(s_CoSetProxyBlanket));
  m_CoSetProxyBlanket_hook->enable();

  m_CoCreateInstance_hook
    = new Function_Hook(handle,
                        "CoCreateInstance",
                        reinterpret_cast<intptr_t>(s_CoCreateInstance));
  m_CoCreateInstance_hook->enable();
}


DotNet::~DotNet()
{
}


void* WINAPI
DotNet::s_CoSetProxyBlanket(void** pProxy, unsigned dwAuthnSvc, unsigned dwAuthzSvc, void* pServerPrincName, unsigned dwAuthnLevel, unsigned dwImpLevel, void* pAuthInfo, unsigned dwCapabilities)
{
  return s_dotnet_instance->CoSetProxyBlanket(pProxy, dwAuthnSvc, dwAuthzSvc, pServerPrincName, dwAuthnLevel, dwImpLevel, pAuthInfo, dwCapabilities);
}

void*
DotNet::CoSetProxyBlanket(void** pProxy, unsigned dwAuthnSvc, unsigned dwAuthzSvc, void* pServerPrincName, unsigned dwAuthnLevel, unsigned dwImpLevel, void* pAuthInfo, unsigned dwCapabilities)
{
  Service::instance().info("coSetProxyBlanket");

  // Call the orignal CoSetProxyBlanket
  m_CoSetProxyBlanket_hook->disable();

  auto orignal_fun = reinterpret_cast<decltype(&s_CoSetProxyBlanket)>(m_CoSetProxyBlanket_hook->get_original());
  auto result = orignal_fun(pProxy, dwAuthnSvc, dwAuthzSvc, pServerPrincName, dwAuthnLevel, dwImpLevel, pAuthInfo, dwCapabilities);

  m_CoSetProxyBlanket_hook->enable();

  // Get virtual table of IWbemServices
  m_wbem_vtbl = reinterpret_cast<IWbemServicesVtbl*>(*pProxy);
  m_wbem_original_vtbl = *m_wbem_vtbl;
  Function_Hook::enable_write_on_page(m_wbem_vtbl);

  // Modify some methods of IWbemServices objects
  m_wbem_vtbl->CreateClassEnum    = reinterpret_cast<intptr_t>(s_CreateClassEnum);
  m_wbem_vtbl->CreateInstanceEnum = reinterpret_cast<intptr_t>(s_CreateInstanceEnum);

  return result;
}


void* WINAPI
DotNet::s_CreateClassEnum(void* This, const wchar_t* strFilter, long lFlags, void* pCtx, void*** ppEnum)
{
  return s_dotnet_instance->CreateInstanceEnum(This, strFilter, lFlags, pCtx, ppEnum);
}


void* WINAPI
DotNet::s_CreateInstanceEnum(void* This, const wchar_t* strFilter, long lFlags, void* pCtx, void*** ppEnum)
{
  return s_dotnet_instance->CreateInstanceEnum(This, strFilter, lFlags, pCtx, ppEnum);
}


void*
DotNet::CreateInstanceEnum(void* This, const wchar_t* strFilter, long lFlags, void* pCtx, void*** ppEnum)
{
  // Call the orignal CreateInstanceEnum
  auto orignal_fun = reinterpret_cast<decltype(&s_CreateInstanceEnum)>(m_wbem_original_vtbl.CreateInstanceEnum);
  auto result = orignal_fun(This, strFilter, lFlags, pCtx, ppEnum);


  std::wstring bstrClassName_s(strFilter);
  if (bstrClassName_s != L"Win32_PNPEntity")
    return result;

  if (m_enum_wbem_vtbl == nullptr)
  {
    // Get virtual table of IEnumWbemClass
    m_enum_wbem_vtbl = reinterpret_cast<IEnumWbemClassObjectVtbl*>(**ppEnum);
    m_enum_wbem_original_vtbl = *m_enum_wbem_vtbl;
    Function_Hook::enable_write_on_page(m_enum_wbem_vtbl);

    // Modify next method of IEnumWbemClassObject objects
    m_enum_wbem_vtbl->Next = reinterpret_cast<intptr_t>(s_Next);
  }

  return result;
}




void* WINAPI
DotNet::s_Next(void* This, uint32_t lTimeout, uint32_t uCount, void*** apObjects, uint32_t* puReturned)
{
  return s_dotnet_instance->Next(This, lTimeout, uCount, apObjects, puReturned);
}

void*
DotNet::Next(void* This, uint32_t lTimeout, uint32_t uCount, void*** apObjects, uint32_t* puReturned)
{
  auto orignal_fun = reinterpret_cast<decltype(&s_Next)>(m_enum_wbem_original_vtbl.Next);
  auto result = orignal_fun(This, lTimeout, uCount, apObjects, puReturned);

  if (*puReturned <= uCount)
  {
    m_enum_wbem_vtbl->Next = m_enum_wbem_original_vtbl.Next; // Disable hook

    // Create a pseudo IWbemClassObject
    size_t num_members_IWbemClassObject = sizeof(IWbemClassObjectVtbl)/sizeof(uintptr_t);
    uintptr_t* fake_object = new uintptr_t[num_members_IWbemClassObject + 1];

    // Set the virtual table to the data of fake_object
    auto pObjects_vtbl = reinterpret_cast<IWbemClassObjectVtbl*>(fake_object+1);
    fake_object[0] = reinterpret_cast<uintptr_t>(pObjects_vtbl);

    // Set all methods to a safe call which do nothing
    for (size_t i = 0; i < num_members_IWbemClassObject; ++i)
    {
      fake_object[i+1] = reinterpret_cast<uintptr_t>(s_Safe);
    }

    // Set Get and Release methods, we don't care about other methods
    pObjects_vtbl->Get     = reinterpret_cast<uintptr_t>(s_Get);
    pObjects_vtbl->Release = reinterpret_cast<uintptr_t>(s_Release);

    // Put the fake object in the vector of object and set size to 1
    **apObjects = reinterpret_cast<void*>(fake_object);
    *puReturned = 1;
  }

  return result;
}


bool WINAPI
DotNet::s_Get(void* This, const wchar_t* wszName, long lFlags, VARIANT *pVal, void* pType, long* plFlavor)
{
  (void) This; (void) lFlags; (void) pType; (void) plFlavor;
  std::wstring wszName_s(wszName);
  if (wszName_s != L"DeviceID")
    return false;

  pVal->vt = /*VT_BSTR*/8;
  pVal->STR = L"VID_3ED9&PID_9E57&IG_00";

  return 0;
}


uint32_t WINAPI
DotNet::s_Release(uintptr_t* This)
{
  delete[] This;
  return 0;
}


void* WINAPI
DotNet::s_Safe(uint8_t*)
{
  // Must never happen
  Service::instance().info("DOTNET : Call of safe function, must never happen");
  return nullptr;
}





//DEFINE_GUID(IID_IDirectInput8A, 0xBF798030,0x483A,0x4DA2,0xAA,0x99,0x5D,0x64,0xED,0x36,0x97,0x00);

int32_t WINAPI
DotNet::s_CoCreateInstance(GUID* rclsid, void* pUnkOuter, int32_t dwClsContext, REFIID riid, uintptr_t** ppv)
{
  return s_dotnet_instance->CoCreateInstance(rclsid, pUnkOuter, dwClsContext, riid, ppv);
}


int32_t
DotNet::CoCreateInstance(GUID* rclsid, void* pUnkOuter, int32_t dwClsContext, REFIID riid, uintptr_t** ppv)
{
  m_CoCreateInstance_hook->disable();
  auto orignal_fun = reinterpret_cast<decltype(&s_CoCreateInstance)>(m_CoCreateInstance_hook->get_original());
  auto result = orignal_fun(rclsid, pUnkOuter, dwClsContext, riid, ppv);
  m_CoCreateInstance_hook->enable();

  // CLSID_DirectInput8
  if (m_dinput8_vtbl == nullptr && rclsid->Data1 == 0x25e609e4)
  {
    hook_IDirectInput(ppv);
    m_CoCreateInstance_hook->disable();
  }

  return result;
}


void
DotNet::s_hook_IDirectInput(uintptr_t** ppv)
{
  s_dotnet_instance->hook_IDirectInput(ppv);
}


void
DotNet::hook_IDirectInput(uintptr_t** ppv)
{
  Service::instance().info("hook IDirectInput");

  m_dinput8_vtbl = reinterpret_cast<IDirectInput8AVtbl*>(**ppv);
  m_dinput8_original_vtbl = *m_dinput8_vtbl;
  Function_Hook::enable_write_on_page(m_dinput8_vtbl);

  m_dinput8_vtbl->Initialize   = reinterpret_cast<intptr_t>(s_IDirectInput8_Initialize);
  m_dinput8_vtbl->EnumDevices  = reinterpret_cast<intptr_t>(s_IDirectInput8_EnumDevices);
  m_dinput8_vtbl->CreateDevice = reinterpret_cast<intptr_t>(s_IDirectInput8_CreateDevice);
}


int32_t WINAPI
DotNet::s_IDirectInput8_Initialize(uintptr_t This, uintptr_t instance, int32_t version)
{
  return s_dotnet_instance->IDirectInput8_Initialize(This, instance, version);
}


int32_t
DotNet::IDirectInput8_Initialize(uintptr_t This, uintptr_t instance, int32_t version)
{
  Service::instance().info("IDirectInput8_Initialize");

  auto orignal_fun = reinterpret_cast<decltype(&s_IDirectInput8_Initialize)>(m_dinput8_original_vtbl.Initialize);
  auto result = orignal_fun(This, instance, version);

  return result;
}


int32_t WINAPI
DotNet::s_IDirectInput8_EnumDevices(uintptr_t This, int32_t dwDevType, DI::EnumDevices_CB lpCallback, void* pvRef, int32_t dwFlags)
{
  return s_dotnet_instance->IDirectInput8_EnumDevices(This, dwDevType, lpCallback, pvRef, dwFlags);
}

struct enum_devices_param
{
  DI::EnumDevices_CB lpCallback;
  void* pvRef;
};

static bool WINAPI
enum_devices_param_callback(DI::DeviceInstanceW* dev, void* pvRef)
{
  enum_devices_param p = *(enum_devices_param*)pvRef;

  // Just send mouse and keyboard
  if ( ((dev->dwDevType & DI::DevType::Mouse)    != DI::DevType::Mouse) &&
       ((dev->dwDevType & DI::DevType::Keyboard) != DI::DevType::Keyboard) )
  {
    GUID xbox_360  = { 0x05C4054C, 0x0000, 0x0000, {0x00, 0x00, 0x50, 0x49, 0x44, 0x56, 0x49, 0x44}};

    //dev->guidInstance = ff_driver;
    dev->guidProduct  = xbox_360;
    //dev->dwDevType = static_cast<DI::DevType>(0x18 + (3 << 8));
  }

  std::u16string instance_name(dev->tszInstanceName);
  std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> cv;
  Service::instance().info() << "found : " << cv.to_bytes(instance_name) << std::endl;

  return p.lpCallback(dev, p.pvRef);
}


int32_t
DotNet::IDirectInput8_EnumDevices(uintptr_t This, int32_t dwDevType, DI::EnumDevices_CB lpCallback, void* pvRef, int32_t dwFlags)
{
  Service::instance().info("IDirectInput8_EnumDevices");

  enum_devices_param p;
  p.lpCallback = lpCallback;
  p.pvRef = pvRef;

  auto orignal_fun = reinterpret_cast<decltype(&s_IDirectInput8_EnumDevices)>(m_dinput8_original_vtbl.EnumDevices);
  auto result = 0;//orignal_fun(This, dwDevType, &enum_devices_param_callback, &p, dwFlags);


  GUID xbox_360  = { 0x05c4054c, 0x0000, 0x0000, {0x00, 0x00, 0x50, 0x49, 0x44, 0x56, 0x49, 0x44}};
  GUID ff_driver = { 0x12345678, 0x0000, 0x0000, {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}};
  const char16_t xbox360_name[] = u"XBOX 360 For Windows (Controller)";

  // Add our device
  DI::DeviceInstanceW device;
  device.dwSize       = sizeof(DI::DeviceInstanceW);
  device.guidInstance = xbox_360;
  device.guidProduct  = xbox_360;
  device.dwDevType    = DI::DevType::Joystick + DI::DevType::JOYSTICK_GAMEPAD;
  memcpy(device.tszInstanceName, xbox360_name, sizeof(xbox360_name));
  memcpy(device.tszProductName,  xbox360_name, sizeof(xbox360_name));
  device.guidFFDriver = ff_driver;
  device.wUsagePage = 0;
  device.wUsage = 0;

  lpCallback(&device, pvRef);

  return result;
}


int32_t WINAPI
DotNet::s_IDirectInput8_CreateDevice(uintptr_t This, REFGUID rguid, void* lplpDirectInputDevice, void* pUnkOuter)
{
  return s_dotnet_instance->IDirectInput8_CreateDevice(This, rguid, lplpDirectInputDevice, pUnkOuter);
}

int32_t
DotNet::IDirectInput8_CreateDevice(uintptr_t This, REFGUID rguid, void* lplpDirectInputDevice, void* pUnkOuter)
{
  Service::instance().info("IDirectInput8_CreateDevice");

  GUID xbox_360  = { 0x05c4054c, 0x0000, 0x0000, {0x00, 0x00, 0x50, 0x49, 0x44, 0x56, 0x49, 0x44}};
  if (*rguid == xbox_360)
  {
    Service::instance().info("IDirectInput8_CreateDevice custom");
    *reinterpret_cast<IDirectInputDevice**>(lplpDirectInputDevice) = new IDirectInputDevice();
    return 0;
  }

  auto orignal_fun = reinterpret_cast<decltype(&s_IDirectInput8_CreateDevice)>(m_dinput8_original_vtbl.CreateDevice);
  auto result = orignal_fun(This, rguid, lplpDirectInputDevice, pUnkOuter);

  return result;
}


} /* namespace Plasium */
