/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/xinput.hpp>

#include <manager.hpp>
#include <service.hpp>
#include <device.hpp>

#include <iostream>
#include <memory>
#include <cstring>
#include <limits>

namespace Plasium
{
namespace XInput
{



Caps
operator|(const Caps left, const Caps right)
{
  return static_cast<Caps>(static_cast<uint16_t>(left)|static_cast<uint16_t>(right));
}

Buttons&
operator|=(Buttons& set, const Buttons& b)
{
  set = static_cast<Buttons>(static_cast<uint16_t>(set)|static_cast<uint16_t>(b));
  return set;
}

/*
  WINMembers

  VirtualKey

  Virtual-key code of the key, button, or stick movement. See XInput.h for a list of valid virtual-key (VK_xxx) codes. Also, see Remarks.
  Unicode

  This member is unused and the value is zero.
  Flags

  Flags that indicate the keyboard state at the time of the input event. This member can be any combination of the following flags:
  Value	Description
  XINPUT_KEYSTROKE_KEYDOWN	The key was pressed.
  XINPUT_KEYSTROKE_KEYUP	The key was released.
  XINPUT_KEYSTROKE_REPEAT	A repeat of a held key.


  UserIndex

  Index of the signed-in gamer associated with the device. Can be a value in the range 0–3.
  HidCode

  HID code corresponding to the input. If there is no corresponding HID code, this value is zero.

  Remarks

  Future devices may return HID codes and virtual key values that are not supported on current devices, and are currently undefined. Applications should ignore these unexpected values.

  A virtual-key code is a byte value that represents a particular physical key on the keyboard, not the character or characters (possibly none) that the key can be mapped to based on keyboard state. The keyboard state at the time a virtual key is pressed modifies the character reported. For example, VK_4 might represent a "4" or a "$", depending on the state of the SHIFT key.

  A reported keyboard event includes the virtual key that caused the event, whether the key was pressed or released (or is repeating), and the state of the keyboard at the time of the event. The keyboard state includes information about whether any CTRL, ALT, or SHIFT keys are down.

  If the keyboard event represents an Unicode character (for example, pressing the "A" key), the Unicode member will contain that character. Otherwise, Unicode will contain the value zero.

  The valid virtual-key (VK_xxx) codes are defined in XInput.h. In addition to codes that indicate key presses, the following codes indicate controller input.
  Value	Description
  VK_PAD_A	A button
  VK_PAD_B	B button
  VK_PAD_X	X button
  VK_PAD_Y	Y button
  VK_PAD_RSHOULDER	Right shoulder button
  VK_PAD_LSHOULDER	Left shoulder button
  VK_PAD_LTRIGGER	Left trigger
  VK_PAD_RTRIGGER	Right trigger
  VK_PAD_DPAD_UP	Directional pad up
  VK_PAD_DPAD_DOWN	Directional pad down
  VK_PAD_DPAD_LEFT	Directional pad left
  VK_PAD_DPAD_RIGHT	Directional pad right
  VK_PAD_START	START button
  VK_PAD_BACK	BACK button
  VK_PAD_LTHUMB_PRESS	Left thumbstick click
  VK_PAD_RTHUMB_PRESS	Right thumbstick click
  VK_PAD_LTHUMB_UP	Left thumbstick up
  VK_PAD_LTHUMB_DOWN	Left thumbstick down
  VK_PAD_LTHUMB_RIGHT	Left thumbstick right
  VK_PAD_LTHUMB_LEFT	Left thumbstick left
  VK_PAD_LTHUMB_UPLEFT	Left thumbstick up and left
  VK_PAD_LTHUMB_UPRIGHT	Left thumbstick up and right
  VK_PAD_LTHUMB_DOWNRIGHT	Left thumbstick down and right
  VK_PAD_LTHUMB_DOWNLEFT	Left thumbstick down and left
  VK_PAD_RTHUMB_UP	Right thumbstick up
  VK_PAD_RTHUMB_DOWN	Right thumbstick down
  VK_PAD_RTHUMB_RIGHT	Right thumbstick right
  VK_PAD_RTHUMB_LEFT	Right thumbstick left
  VK_PAD_RTHUMB_UPLEFT	Right thumbstick up and left
  VK_PAD_RTHUMB_UPRIGHT	Right thumbstick up and right
  VK_PAD_RTHUMB_DOWNRIGHT	Right thumbstick down and right
  VK_PAD_RTHUMB_DOWNLEFT	Right thumbstick down and left

*/

typedef struct _XINPUT_KEYSTROKE {
  WORD  VirtualKey;
  WCHAR Unicode;
  WORD  Flags;
  BYTE  UserIndex;
  BYTE  HidCode;
} XINPUT_KEYSTROKE, *PXINPUT_KEYSTROKE;
/*
  Members

  dwPacketNumber

  State packet number. The packet number indicates whether there have been any changes in the state of the controller. If the dwPacketNumber member is the same in sequentially returned XINPUT_STATE structures, the controller state has not changed.
  Gamepad

  XINPUT_GAMEPAD structure containing the current state of an Xbox 360 Controller.

  typedef struct _XINPUT_STATE {
  DWORD     dwPacketNumber;
  Gamepad   gamepad;
  } XINPUT_STATE, *PXINPUT_STATE;
*/

// The Gamepad manager used by all function of XInput
static std::vector<Device*> s_list_devices;


void WINAPI
Enable(bool)
{
  if (s_list_devices.empty())
  {
    Service::instance().info("XInput Enable");
    s_list_devices = Manager::instance().get_devices();
  }
}



uint32_t WINAPI
GetAudioDeviceIds(uint32_t dwUserIndex,
                  char16_t* pRenderDeviceId,  uint32_t* pRenderCount,
                  char16_t* pCaptureDeviceId, uint32_t* pCaptureCount)
{
  Service::instance().info("XInput GetAudioDeviceIds");

  (void) pRenderDeviceId; (void) pCaptureDeviceId;
  if (dwUserIndex >= s_list_devices.size())
  {
    return to_int(Error_Code::Device_Not_Connected);
  }

  // Consider no audio headset
  if (pRenderCount)
    *pRenderCount  = 0;
  if (pCaptureCount)
    *pCaptureCount = 0;

  return to_int(Error_Code::Success);
}



uint32_t WINAPI
GetBatteryInformation(uint32_t dwUserIndex, uint8_t devType,
                      BatteryInformation* pBatteryInformation)
{
  Service::instance().info("XInput GetBatteryInformation");

  (void)devType;
  if (dwUserIndex >= s_list_devices.size())
  {
    return to_int(Error_Code::Device_Not_Connected);
  }

  // Consider all devices as wired with full battery
  pBatteryInformation->level = BatteryLevel::FULL;
  pBatteryInformation->type  = BatteryType::WIRED;

  return to_int(Error_Code::Success);
}



uint32_t WINAPI
GetCapabilities(uint32_t dwUserIndex, uint32_t dwFlags,
                Capabilities* pCapabilities)
{
  Service::instance().info("XInput GetCapabilities");

  if (dwUserIndex >= s_list_devices.size())
  {
    return to_int(Error_Code::Device_Not_Connected);
  }

  (void) dwFlags; /* XINPUT_FLAG_GAMEPAD is the only valid value, don't need to check */

  pCapabilities->type     = XINPUT_DEVTYPE_GAMEPAD;
  pCapabilities->sub_type = DevSubType::Gamepad;
  pCapabilities->flags    = Caps::None;

  // Capabilities and default state of the controller
  pCapabilities->gamepad.wButtons      = Buttons::Full;
  pCapabilities->gamepad.bLeftTrigger  = 0;
  pCapabilities->gamepad.bRightTrigger = 0;
  pCapabilities->gamepad.sThumbLX      = 0;
  pCapabilities->gamepad.sThumbLY      = 0;
  pCapabilities->gamepad.sThumbRX      = 0;
  pCapabilities->gamepad.sThumbRY      = 0;

  // Don't support vibration for now
  pCapabilities->vibration.wLeftMotorSpeed  = 0;
  pCapabilities->vibration.wRightMotorSpeed = 0;

  return to_int(Error_Code::Success);
}


uint32_t WINAPI
GetDSoundAudioDeviceGuids(uint32_t dwUserIndex,
                          GUID* pDSoundRenderGuid, GUID* pDSoundCaptureGuid)
{
  Service::instance().info("XInput GetDSoundAudioDeviceGuids");

  if (dwUserIndex >= s_list_devices.size())
  {
    return to_int(Error_Code::Device_Not_Connected);
  }


  if (pDSoundRenderGuid)
    std::memset(pDSoundRenderGuid,  0, sizeof(GUID));
  if (pDSoundCaptureGuid)
    std::memset(pDSoundCaptureGuid, 0, sizeof(GUID));


  return to_int(Error_Code::Success);
}

/* TODO :
  XInputGetKeystroke


  Retrieves a gamepad input event.
*/
uint32_t WINAPI GetKeystroke(
   DWORD             dwUserIndex,
   DWORD             dwReserved,
   /*PXINPUT_KEYSTROKE* */ void* pKeystroke)
{
  Service::instance().info("XInput GetKeystroke");
  (void) dwReserved; (void) pKeystroke;

  if (dwUserIndex >= s_list_devices.size())
  {
    return to_int(Error_Code::Device_Not_Connected);
  }

  return to_int(Error_Code::Success);
}



uint32_t WINAPI
GetState(uint32_t dwUserIndex,
         State *pState)
{
  static int pck_num = 0;
  Enable(true);
  if (dwUserIndex >= s_list_devices.size())
  {
    return to_int(Error_Code::Device_Not_Connected);
  }

  const Gamepad_State& state = s_list_devices[dwUserIndex]->get_state();

  pState->gamepad.wButtons = Buttons::None;
  pState->gamepad.bLeftTrigger  = std::numeric_limits<uint8_t>::max()*((float)state.axis[Axis_Enum::L2]/std::numeric_limits<int16_t>::max());
  pState->gamepad.bRightTrigger = std::numeric_limits<uint8_t>::max()*((float)state.axis[Axis_Enum::R2]/std::numeric_limits<int16_t>::max());
  pState->gamepad.sThumbLX = state.axis[Axis_Enum::LEFT_X];
  pState->gamepad.sThumbLY = state.axis[Axis_Enum::LEFT_Y];
  pState->gamepad.sThumbRX = state.axis[Axis_Enum::RIGHT_X];
  pState->gamepad.sThumbRY = state.axis[Axis_Enum::RIGHT_Y];

  if ( pState->gamepad.sThumbLY != 0 )
    pState->gamepad.sThumbLY = (-1*pState->gamepad.sThumbLY)-1;
  if ( pState->gamepad.sThumbRY != 0 )
    pState->gamepad.sThumbRY = (-1*pState->gamepad.sThumbRY)-1;


  if ( state.buttons[Buttons_Enum::LEFTCROSS_UP] )
    pState->gamepad.wButtons |= Buttons::DPAD_UP;
  if ( state.buttons[Buttons_Enum::LEFTCROSS_DOWN] )
    pState->gamepad.wButtons |= Buttons::DPAD_DOWN;
  if ( state.buttons[Buttons_Enum::LEFTCROSS_LEFT] )
    pState->gamepad.wButtons |= Buttons::DPAD_LEFT;
  if ( state.buttons[Buttons_Enum::LEFTCROSS_RIGHT] )
    pState->gamepad.wButtons |= Buttons::DPAD_RIGHT;


  if ( state.buttons[Buttons_Enum::RIGHTCROSS_UP] )
    pState->gamepad.wButtons |= Buttons::Y;
  if ( state.buttons[Buttons_Enum::RIGHTCROSS_DOWN] )
    pState->gamepad.wButtons |= Buttons::A;
  if ( state.buttons[Buttons_Enum::RIGHTCROSS_LEFT] )
    pState->gamepad.wButtons |= Buttons::X;
  if ( state.buttons[Buttons_Enum::RIGHTCROSS_RIGHT] )
    pState->gamepad.wButtons |= Buttons::B;

  if ( state.buttons[Buttons_Enum::SELECT] )
    pState->gamepad.wButtons |= Buttons::BACK;
  if ( state.buttons[Buttons_Enum::START] )
    pState->gamepad.wButtons |= Buttons::START;

  if ( state.buttons[Buttons_Enum::L3] )
    pState->gamepad.wButtons |= Buttons::LEFT_THUMB;
  if ( state.buttons[Buttons_Enum::R3] )
    pState->gamepad.wButtons |= Buttons::RIGHT_THUMB;

  if ( state.buttons[Buttons_Enum::L1] )
    pState->gamepad.wButtons |= Buttons::LEFT_SHOULDER;
  if ( state.buttons[Buttons_Enum::R1] )
    pState->gamepad.wButtons |= Buttons::RIGHT_SHOULDER;

  pState->dwPacketNumber = pck_num++;

  return to_int(Error_Code::Success);
}


uint32_t WINAPI
SetState(uint32_t dwUserIndex, Vibration *pVibration)
{
  Service::instance().info("XInput SetState");

  (void) pVibration;

  if (dwUserIndex >= s_list_devices.size())
  {
    return to_int(Error_Code::Device_Not_Connected);
  }

  return to_int(Error_Code::Success);
}

} /* namespace XInput */
} /* namespace Plasium */
