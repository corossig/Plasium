/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/function_hook.hpp>

#include <iostream>
#include <cstring>
#include <stdexcept>
#include <unistd.h>
#include <sys/mman.h>
#include <dlfcn.h>

namespace Plasium
{

Function_Hook::Function_Hook(void* wine_handle, const char* original, intptr_t faked)
  : m_faked(faked)
{
  // Find address of the original function
  m_original = reinterpret_cast<intptr_t>(::dlsym(wine_handle, original));
  if (!m_original)
    throw std::runtime_error(std::string("Can't found function ") + original);


  m_jump_sequence.op_code = 0xE9; //< Jmp op code in X86
  m_jump_sequence.relative_addr = faked - m_original - sizeof(m_jump_sequence); //< Compute relative jump to faked, we need to substract the sizeof of the jump_sequence because ESP point just after it

  set_page_writable();
  std::memcpy(static_cast<void*>(m_saved_code),
              reinterpret_cast<void*>(m_original),
              sizeof(jump_ins));
}


Function_Hook::Function_Hook(intptr_t original, intptr_t faked)
  : m_original(original)
  , m_faked(faked)
{
  m_jump_sequence.op_code = 0xE9; //< Jmp op code in X86
  m_jump_sequence.relative_addr = m_original-faked-sizeof(m_jump_sequence); //< Compute relative jump to faked, we need to substract the sizeof of the jump_sequence because ESP point just after it

  set_page_writable();
  std::memcpy(static_cast<void*>(m_saved_code),
              reinterpret_cast<void*>(m_original),
              sizeof(jump_ins));
}


void
Function_Hook::enable_write_on_page(intptr_t addr)
{
  size_t page_size = getpagesize();

  intptr_t addr_start = addr - (addr%page_size);

  mprotect(reinterpret_cast<void*>(addr_start), page_size, PROT_READ|PROT_WRITE|PROT_EXEC);
}

void
Function_Hook::enable_write_on_page(void* addr)
{
  enable_write_on_page(reinterpret_cast<intptr_t>(addr));
}

void
Function_Hook::enable()
{
  std::memcpy(reinterpret_cast<void*>(m_original),
              reinterpret_cast<void*>(&m_jump_sequence),
              sizeof(jump_ins));
}


void
Function_Hook::disable()
{
  std::memcpy(reinterpret_cast<void*>(m_original),
              static_cast<void*>(m_saved_code),
              sizeof(jump_ins));
}


intptr_t
Function_Hook::get_original() const
{
  return m_original;
}


void
Function_Hook::set_page_writable()
{
  enable_write_on_page(m_original);
}


} /* namespace Plasium */
