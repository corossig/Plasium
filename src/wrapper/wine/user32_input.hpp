/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __USER_INPUT_HPP__
#define __USER_INPUT_HPP__

#include <wrapper/wine/win_typedef.hpp>
#include <wrapper/wine/function_hook.hpp>

#include <map>
#include <string>

typedef struct tagRAWINPUTDEVICELIST {
  uintptr_t hDevice;
  DWORD     dwType;
} RAWINPUTDEVICELIST, *PRAWINPUTDEVICELIST;

typedef struct tagRAWINPUTDEVICE {
    uint16_t usUsagePage;
    uint16_t usUsage;
    uint32_t dwFlags;
    void*    hwndTarget;
} RAWINPUTDEVICE, *PRAWINPUTDEVICE, *LPRAWINPUTDEVICE;
typedef struct tagRAWHID {
  uint32_t dwSizeHid;
  uint32_t dwCount;
  uint8_t  bRawData[1];
} RAWHID, *PRAWHID, *LPRAWHID;

typedef struct tagRAWINPUTHEADER {
  uint32_t  dwType;
  uint32_t  dwSize;
  void* hDevice;
  uint32_t* wParam;
} RAWINPUTHEADER, *PRAWINPUTHEADER;

typedef struct tagRAWMOUSE {
  uint16_t usFlags;
  union {
    uint32_t  ulButtons;
    struct {
      uint16_t usButtonFlags;
      uint16_t usButtonData;
    } flag_data;
  };
  uint32_t  ulRawButtons;
  int32_t   lLastX;
  int32_t   lLastY;
  uint32_t  ulExtraInformation;
} RAWMOUSE, *PRAWMOUSE, *LPRAWMOUSE;

typedef struct tagRAWKEYBOARD {
  uint16_t MakeCode;
  uint16_t Flags;
  uint16_t Reserved;
  uint16_t VKey;
  uint32_t   Message;
  uint32_t  ExtraInformation;
} RAWKEYBOARD, *PRAWKEYBOARD, *LPRAWKEYBOARD;


typedef struct tagRAWINPUT {
  RAWINPUTHEADER header;
  union {
    RAWMOUSE    mouse;
    RAWKEYBOARD keyboard;
    RAWHID      hid;
  } data;
} RAWINPUT, *PRAWINPUT, *LPRAWINPUT;


typedef struct tagRID_DEVICE_INFO_MOUSE {
  uint32_t dwId;
  uint32_t dwNumberOfButtons;
  uint32_t dwSampleRate;
  bool     fHasHorizontalWheel;
} RID_DEVICE_INFO_MOUSE, *PRID_DEVICE_INFO_MOUSE;


typedef struct tagRID_DEVICE_INFO_KEYBOARD {
  uint32_t dwType;
  uint32_t dwSubType;
  uint32_t dwKeyboardMode;
  uint32_t dwNumberOfFunctionKeys;
  uint32_t dwNumberOfIndicators;
  uint32_t dwNumberOfKeysTotal;
} RID_DEVICE_INFO_KEYBOARD, *PRID_DEVICE_INFO_KEYBOARD;



typedef struct tagRID_DEVICE_INFO_HID {
  uint32_t  dwVendorId;
  uint32_t  dwProductId;
  uint32_t  dwVersionNumber;
  uint16_t  usUsagePage;
  uint16_t  usUsage;
} RID_DEVICE_INFO_HID, *PRID_DEVICE_INFO_HID;

typedef struct tagRID_DEVICE_INFO {
  uint32_t cbSize;
  uint32_t dwType;
  union {
    RID_DEVICE_INFO_MOUSE    mouse;
    RID_DEVICE_INFO_KEYBOARD keyboard;
    RID_DEVICE_INFO_HID      hid;
  };
} RID_DEVICE_INFO, *PRID_DEVICE_INFO, *LPRID_DEVICE_INFO;



namespace Plasium
{
enum class RIDI;
enum class RIMTYPE;

class U32_Input
{
public :
  static bool WINAPI RegisterRawInputDevices(RAWINPUTDEVICE *devices, uint32_t device_count, uint32_t size);

  static uint32_t WINAPI GetRawInputDeviceList(RAWINPUTDEVICELIST *devices, uint32_t *device_count, uint32_t size);

  static uint32_t WINAPI GetRawInputDeviceInfo(uintptr_t hDevice, RIDI uiCommand, void* pData, uint32_t* pcbSize);

  static uint32_t WINAPI GetRawInputData(RAWINPUT* hRawInput, uint32_t uiCommand,
                                         void* pData, uint32_t* pcbSize, uint32_t cbSizeHeader);

  /*
  static bool WINAPI PostMessageA(void* hwnd, uint32_t msg, WPARAM wparam, LPARAM lparam )
BOOL WINAPI PostMessageW( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam )
  */


  /* DLL HID */

  /*
  NTSTATUS __stdcall HidP_GetValueCaps(
  _In_    HIDP_REPORT_TYPE     ReportType,
  _Out_   PHIDP_VALUE_CAPS     ValueCaps,
  _Inout_ Puint16_t              ValueCapsLength,
  _In_    PHIDP_PREPARSED_DATA PreparsedData
);
  */
  /*
NTSTATUS __stdcall HidP_GetUsages(
  _In_    HIDP_REPORT_TYPE     ReportType,
  _In_    USAGE                UsagePage,
  _In_    uint16_t               LinkCollection,
  _Out_   PUSAGE               UsageList,
  _Inout_ Puint32_t               UsageLength,
  _In_    PHIDP_PREPARSED_DATA PreparsedData,
  _Out_   PCHAR                Report,
  _In_    uint32_t                ReportLength
);
  */


  /*
NTSTATUS __stdcall HidP_GetUsageValue(
  _In_  HIDP_REPORT_TYPE     ReportType,
  _In_  USAGE                UsagePage,
  _In_  uint16_t               LinkCollection,
  _In_  USAGE                Usage,
  _Out_ Puint32_t               UsageValue,
  _In_  PHIDP_PREPARSED_DATA PreparsedData,
  _In_  PCHAR                Report,
  _In_  uint32_t                ReportLength
);
  */
  static uint32_t WINAPI (*s_PostMessageW)(uintptr_t, int32_t, void*, void*);

  static std::map<std::string, Plasium::Function_Hook> s_hooks;
};

} /* namespace Plasium */

#endif /* __USER_INPUT_HPP__ */
