/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <stdexcept>
#include <dlfcn.h>


#include <wrapper/wine/xinput.hpp>
#include <wrapper/wine/win_typedef.hpp>
#include <wrapper/wine/user32_input.hpp>
#include <wrapper/wine/direct_input.hpp>
#include <wrapper/wine/dotnet_device.hpp>
#include <wrapper/wine/hidp.hpp>
#include <wrapper/wine/function_hook.hpp>
#include <service.hpp>




#include <iostream>
#include <cstring>
#include <stdexcept>
#include <unistd.h>
#include <sys/mman.h>
#include <dlfcn.h>


extern "C"
void*
wine_dll_load(const char *filename_, char *error, int errorsize, int *file_exists)
{
  static std::vector<Plasium::Function_Hook> m_hooks;

  std::string filename(filename_);

  // call original function
  void* original_result = ((decltype(&wine_dll_load))dlsym(RTLD_NEXT, "wine_dll_load"))(filename_, error, errorsize, file_exists);

  //check for dlls
  if (filename.find("xinput") != std::string::npos)
  {
    std::initializer_list<std::pair<const char*, intptr_t>> xinput_list =
      {
        {"XInputEnable"                    , reinterpret_cast<intptr_t>(&Plasium::XInput::Enable)},
        {"XInputGetAudioDeviceIds"         , reinterpret_cast<intptr_t>(&Plasium::XInput::GetAudioDeviceIds)},
        {"XInputGetBatteryInformation"     , reinterpret_cast<intptr_t>(&Plasium::XInput::GetBatteryInformation)},
        {"XInputGetCapabilities"           , reinterpret_cast<intptr_t>(&Plasium::XInput::GetCapabilities)},
        {"XInputGetDSoundAudioDeviceGuids" , reinterpret_cast<intptr_t>(&Plasium::XInput::GetDSoundAudioDeviceGuids)},
        {"XInputGetKeystroke"              , reinterpret_cast<intptr_t>(&Plasium::XInput::GetKeystroke)},
        {"XInputGetState"                  , reinterpret_cast<intptr_t>(&Plasium::XInput::GetState)},
        {"XInputSetState"                  , reinterpret_cast<intptr_t>(&Plasium::XInput::SetState)}
      };

    for (const auto& func : xinput_list)
    {
      //hook functions
      try
      {/*
        std::unique_ptr<Plasium::Function_Hook> f_hook(new Plasium::Function_Hook(original_result, func.first, func.second));
        f_hook->enable();
        Plasium::U32_Input::s_hooks.emplace(func.first, std::move(f_hook));*/
        Plasium::Function_Hook f_hook(original_result, func.first, func.second);
        f_hook.enable();
        Plasium::U32_Input::s_hooks.emplace(func.first, std::move(f_hook));
      }
      catch (const std::runtime_error& e)
      {
        Plasium::Service::instance().warning() << e.what()  << " in " << filename << std::endl;
      }
    }
  }


  if (filename.find("dinput8") != std::string::npos)
  {
    std::initializer_list<std::pair<const char*, intptr_t>> u32_input_list =
      {
        {"DirectInput8Create",       reinterpret_cast<intptr_t>(&Plasium::DInput::DirectInput8Create)}
      };

    for (const auto& func : u32_input_list)
    {
      //hook functions
      try
      {
        Plasium::Function_Hook f_hook(original_result, func.first, func.second);
        f_hook.enable();
        Plasium::DInput::s_hooks.emplace(func.first, std::move(f_hook));
      }
      catch (const std::runtime_error& e)
      {
        Plasium::Service::instance().warning() << e.what() << " in " << filename << std::endl;
      }
    }
  }


  if (filename.find("user32") != std::string::npos)
  {
    std::initializer_list<std::pair<const char*, intptr_t>> u32_input_list =
      {
        {"GetRawInputDeviceList",   reinterpret_cast<intptr_t>(&Plasium::U32_Input::GetRawInputDeviceList)},
        {"GetRawInputDeviceInfoA",  reinterpret_cast<intptr_t>(&Plasium::U32_Input::GetRawInputDeviceInfo)},
        {"GetRawInputDeviceInfoW",  reinterpret_cast<intptr_t>(&Plasium::U32_Input::GetRawInputDeviceInfo)},
        {"RegisterRawInputDevices", reinterpret_cast<intptr_t>(&Plasium::U32_Input::RegisterRawInputDevices)},
        {"GetRawInputData",         reinterpret_cast<intptr_t>(&Plasium::U32_Input::GetRawInputData)}
      };

    Plasium::U32_Input::s_PostMessageW = reinterpret_cast<uint32_t WINAPI (*)(uintptr_t, int32_t, void*, void*)>(::dlsym(original_result, "PostMessageW"));

    for (const auto& func : u32_input_list)
    {
      //hook functions
      try
      {
        Plasium::Function_Hook f_hook(original_result, func.first, func.second);
        f_hook.enable();
        Plasium::U32_Input::s_hooks.emplace(func.first, std::move(f_hook));
      }
      catch (const std::runtime_error& e)
      {
        Plasium::Service::instance().warning() << e.what() << " in " << filename << std::endl;
      }
    }
  }



  if (filename.find("hid") != std::string::npos)
  {
    Plasium::Service::instance().warning() << " LOADIN " << filename << std::endl;
    std::initializer_list<std::pair<const char*, intptr_t>> u32_input_list =
      {
        {"HidP_GetCaps",       reinterpret_cast<intptr_t>(&Plasium::HidP::GetCaps)},
        {"HidP_GetButtonCaps", reinterpret_cast<intptr_t>(&Plasium::HidP::GetButtonCaps)}
      };

    for (const auto& func : u32_input_list)
    {
      //hook functions
      try
      {
        Plasium::Function_Hook f_hook(original_result, func.first, func.second);
        f_hook.enable();
        Plasium::HidP::s_hooks.emplace(func.first, std::move(f_hook));
        Plasium::Service::instance().warning() << " OK " << func.first << std::endl;
      }
      catch (const std::runtime_error& e)
      {
        Plasium::Service::instance().warning() << e.what() << " in " << filename << std::endl;
      }
    }
  }


  if (filename == "ole32.dll")
  {
    new Plasium::DotNet(original_result);
  }

  return original_result;
}
