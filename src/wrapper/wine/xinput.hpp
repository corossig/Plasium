/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/win_typedef.hpp>



namespace Plasium
{
namespace XInput
{

enum class BatteryType : uint8_t
{
  DISCONNECTED,
  WIRED,
  ALKALINE,
  NIMH,
  UNKNOWN = 0xff
};


enum class BatteryLevel : uint8_t
{
  EMPTY,
  LOW,
  MEDIUM,
  FULL
};


struct BatteryInformation
{
  BatteryType  type;
  BatteryLevel level;
};


/* TODO : Find real values of this enum */
enum class Caps : uint16_t
{
  None = 0,
  VOICE_SUPPORTED,
  FFB_SUPPORTED /*= 1*/,
  WIRELESS,
  PMD_SUPPORTED,
  NO_NAVIGATION
};


struct Vibration
{
  uint16_t wLeftMotorSpeed;
  uint16_t wRightMotorSpeed;
};


enum class Buttons : uint16_t
{
  None           = 0,
  DPAD_UP        = 1<< 0,
  DPAD_DOWN      = 1<< 1,
  DPAD_LEFT      = 1<< 2,
  DPAD_RIGHT     = 1<< 3,
  START          = 1<< 4,
  BACK           = 1<< 5,
  LEFT_THUMB     = 1<< 6,
  RIGHT_THUMB    = 1<< 7,
  LEFT_SHOULDER  = 1<< 8,
  RIGHT_SHOULDER = 1<< 9,
  A              = 1<<12,
  B              = 1<<13,
  X              = 1<<14,
  Y              = 1<<15,
  Full           = 62463
};


struct Gamepad
{
  Buttons  wButtons;
  uint8_t  bLeftTrigger;
  uint8_t  bRightTrigger;
  int16_t  sThumbLX;
  int16_t  sThumbLY;
  int16_t  sThumbRX;
  int16_t  sThumbRY;
};


#define XINPUT_DEVTYPE_GAMEPAD 0x01
enum class DevSubType : uint8_t
{
  Gamepad = 1,
};

struct Capabilities
{
  uint8_t      type;
  DevSubType   sub_type;
  Caps         flags;
  Gamepad      gamepad;
  Vibration    vibration;
};


struct State
{
  uint32_t dwPacketNumber;
  Gamepad  gamepad;
};


void WINAPI Enable(bool enable);

uint32_t WINAPI GetAudioDeviceIds(uint32_t dwUserIndex,
                                  char16_t *pRenderDeviceId,  uint32_t *pRenderCount,
                                  char16_t *pCaptureDeviceId, uint32_t *pCaptureCount);


uint32_t WINAPI GetBatteryInformation(uint32_t dwUserIndex, uint8_t devType,
                                      BatteryInformation* pBatteryInformation);



uint32_t WINAPI GetCapabilities(uint32_t dwUserIndex, uint32_t dwFlags,
                                Capabilities* pCapabilities);


uint32_t WINAPI GetDSoundAudioDeviceGuids(uint32_t dwUserIndex,
                                          GUID* pDSoundRenderGuid, GUID* pDSoundCaptureGuid);


uint32_t WINAPI GetKeystroke(
   DWORD             dwUserIndex,
   DWORD             dwReserved,
   /*PXINPUT_KEYSTROKE* */ void* pKeystroke
);

uint32_t WINAPI GetState(uint32_t dwUserIndex,
                         State *pState);


uint32_t WINAPI SetState(uint32_t dwUserIndex, Vibration *pVibration);

} /* namespace Plasium */
} /* namespace XInput */
