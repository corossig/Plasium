/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DOTNET_DEVICE_HPP__
#define __DOTNET_DEVICE_HPP__

#include <wrapper/wine/dotnet_device_struct.hpp>
#include <wrapper/wine/direct_input_device_typedef.hpp>
#include <wrapper/wine/function_hook.hpp>
#include <wrapper/wine/win_typedef.hpp>

namespace Plasium
{

class DotNet
{
public :
  DotNet(void* handle);
  ~DotNet();

  static void s_hook_IDirectInput(uintptr_t** ppv);

private :
  static void* WINAPI s_CoSetProxyBlanket(void**, uint32_t, uint32_t, void*, uint32_t,uint32_t, void*, uint32_t);
  void* CoSetProxyBlanket(void**, uint32_t, uint32_t, void*, uint32_t,uint32_t, void*, uint32_t);


  static int32_t WINAPI s_CoCreateInstance(GUID* rclsid, void*, int32_t dwClsContext, REFIID riid, uintptr_t** ppv);
  int32_t CoCreateInstance(GUID* rclsid, void*, int32_t dwClsContext, REFIID riid, uintptr_t** ppv);


  static void* WINAPI s_CreateClassEnum(void* This, const wchar_t* strFilter, long lFlags, void* pCtx, void*** ppEnum);
  static void* WINAPI s_CreateInstanceEnum(void* This, const wchar_t* strFilter, long lFlags, void* pCtx, void*** ppEnum);
  void* CreateInstanceEnum(void* This, const wchar_t* strFilter, long lFlags, void* pCtx, void*** ppEnum);


  static void* WINAPI s_Next(void* This, uint32_t lTimeout, uint32_t uCount, void*** apObjects, uint32_t* puReturned);
  void* Next(void* This, uint32_t lTimeout, uint32_t uCount, void*** apObjects, uint32_t* puReturned);

  static bool     WINAPI s_Get(void* This, const wchar_t* wszName, long lFlags, VARIANT *pVal, void* pType, long* plFlavor);
  static uint32_t WINAPI s_Release(uintptr_t*);
  static void*    WINAPI s_Safe(uint8_t*);


  void hook_IDirectInput(uintptr_t** ppv);

  static int32_t WINAPI s_IDirectInput8_Initialize(uintptr_t self, uintptr_t instance, int32_t version);
  int32_t IDirectInput8_Initialize(uintptr_t self, uintptr_t instance, int32_t version);



  static int32_t WINAPI s_IDirectInput8_EnumDevices(uintptr_t This, int32_t dwDevType, DI::EnumDevices_CB lpCallback, void* pvRef, int32_t dwFlags);
  int32_t IDirectInput8_EnumDevices(uintptr_t This, int32_t dwDevType, DI::EnumDevices_CB lpCallback, void* pvRef, int32_t dwFlags);



  static int32_t WINAPI s_IDirectInput8_CreateDevice(uintptr_t This, REFGUID rguid, void* lplpDirectInputDevice, void* pUnkOuter);
  int32_t IDirectInput8_CreateDevice(uintptr_t This, REFGUID rguid, void* lplpDirectInputDevice, void* pUnkOuter);

private :
  IWbemServicesVtbl  m_wbem_original_vtbl;
  IWbemServicesVtbl* m_wbem_vtbl;

  IEnumWbemClassObjectVtbl  m_enum_wbem_original_vtbl;
  IEnumWbemClassObjectVtbl* m_enum_wbem_vtbl;

  IDirectInput8AVtbl  m_dinput8_original_vtbl;
  IDirectInput8AVtbl* m_dinput8_vtbl;

  Function_Hook* m_CoSetProxyBlanket_hook;
  Function_Hook* m_CoCreateInstance_hook;

  static DotNet* s_dotnet_instance;
};

namespace DotNet_Device
{

void Init(void* handle);

} /* namespace DotNet_Device */
} /* namespace Plasium */



#endif /* __DOTNET_DEVICE_HPP__ */
