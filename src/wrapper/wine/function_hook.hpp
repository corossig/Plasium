/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FUNTION_HOOK_HPP__
#define __FUNTION_HOOK_HPP__

#include <cstdint>

namespace Plasium
{

class Function_Hook
{
  // This class replace an existant function in memory by one of our functions
  // This is the same technique as used with plt for Position Independant Code

  struct __attribute__((packed)) jump_ins
  {
    uint8_t  op_code;
    intptr_t relative_addr;
  };

public :
  Function_Hook(void* wine_handle, const char* original, intptr_t faked);
  Function_Hook(intptr_t original, intptr_t faked);

  void enable();
  void disable();

  intptr_t get_original() const;

  static void enable_write_on_page(intptr_t addr);
  static void enable_write_on_page(void* addr);

private :
  void set_page_writable();

private :
  intptr_t m_original;
  intptr_t m_faked;
  jump_ins m_jump_sequence;
  uint8_t  m_saved_code[sizeof(jump_ins)];
};

} /* namespace Plasium */

#endif /* __FUNTION_HOOK_HPP__ */
