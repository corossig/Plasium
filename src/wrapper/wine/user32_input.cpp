/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/wine/user32_input.hpp>

#include <wrapper/wine/function_hook.hpp>

#include <service.hpp>
#include <manager.hpp>

#include <cassert>
#include <cstring>


namespace Plasium
{
enum class RIDI
{
  DeviceName = 0x20000007, /* pData points to a string that contains the device name. */
  /* For this uiCommand only, the value in pcbSize is the character count (not the byte count). */
  DeviceInfo = 0x2000000b, /* pData points to an RID_DEVICE_INFO structure. */
  PreparseData = 0x20000005 /* pData points to the previously parsed data. */
};

enum class RIMTYPE
{
  Mouse = 0,
  Keyboard = 1,
  HID = 2
};

uint32_t WINAPI (*U32_Input::s_PostMessageW)(uintptr_t, int32_t, void*, void*);

std::map<std::string, Plasium::Function_Hook> U32_Input::s_hooks;

bool WINAPI
U32_Input::RegisterRawInputDevices(RAWINPUTDEVICE *devices, uint32_t device_count, uint32_t size)
{
  Service::instance().warning("RegisterRawInputDevices");

  auto& fun_hook = s_hooks.find("RegisterRawInputDevices")->second;
  fun_hook.disable();

  auto orignal_fun = reinterpret_cast<decltype(&RegisterRawInputDevices)>(fun_hook.get_original());
  auto result = orignal_fun(devices, device_count, size);

  fun_hook.enable();

  Service::instance().warning() << "RegisterRawInputDevices : Usage " << devices[0].usUsage << std::endl;
#define WM_INPUT 0x00ff
  RAWINPUT r;
  r.header.dwType = 42;
  r.header.dwSize = sizeof(RAWINPUT);
  r.header.hDevice = (void*)3;
  r.header.wParam = nullptr;



  Service::instance().warning() << "FIND : " << s_PostMessageW << std::endl;
  s_PostMessageW((uintptr_t)0xffff, WM_INPUT, nullptr, &r);

#if 0
  struct rawinput_device *d;
  bool ret = true;
  uint32_t i;

    if (!(d = HeapAlloc( GetProcessHeap(), 0, device_count * sizeof(*d) ))) return FALSE;

    for (i = 0; i < device_count; ++i)
    {
        TRACE("device %u: page %#x, usage %#x, flags %#x, target %p.\n",
                i, devices[i].usUsagePage, devices[i].usUsage,
                devices[i].dwFlags, devices[i].hwndTarget);
        if (devices[i].dwFlags & ~RIDEV_REMOVE)
            FIXME("Unhandled flags %#x for device %u.\n", devices[i].dwFlags, i);

        d[i].usage_page = devices[i].usUsagePage;
        d[i].usage = devices[i].usUsage;
        d[i].flags = devices[i].dwFlags;
        d[i].target = wine_server_user_handle( devices[i].hwndTarget );
    }

    SERVER_START_REQ( update_rawinput_devices )
    {
        wine_server_add_data( req, d, device_count * sizeof(*d) );
        ret = !wine_server_call( req );
    }
    SERVER_END_REQ;

    HeapFree( GetProcessHeap(), 0, d );
#endif
  return result;
}


uint32_t WINAPI
U32_Input::GetRawInputDeviceList(RAWINPUTDEVICELIST *devices, uint32_t *device_count, uint32_t size)
{
  auto& fun_hook = s_hooks.find("GetRawInputDeviceList")->second;
  fun_hook.disable();

  auto orignal_fun = reinterpret_cast<decltype(&GetRawInputDeviceList)>(fun_hook.get_original());
  auto result = orignal_fun(devices, device_count, size);

  fun_hook.enable();

#if 1
  if (devices == nullptr)
  {
    *device_count = 3; /* keyboard + mouse + gamepad */
    return 0;
  }

  devices[0].hDevice = 1;
  devices[0].dwType  = 0;

  devices[2].hDevice = 2;
  devices[2].dwType  = 1;

  devices[0].hDevice = 3;
  devices[0].dwType  = 2;

  Service::instance().info("Add raw gamepad");
#endif

  Service::instance().warning() << "Plasium GetRawInputDeviceList : device_count " << *device_count << " " << size << std::endl;

  return result;
}

#if 0
    RIDI_PREPARSEDDATA
    0x20000005



    pData points to the previously parsed data.

#endif

uint32_t WINAPI
U32_Input::GetRawInputDeviceInfo(uintptr_t hDevice, RIDI uiCommand, void* pData, uint32_t* pcbSize)
{
  auto& dout = Service::instance().warning();
  dout << "Plasium GetRawInputDeviceInfo : " << std::endl;

  if (false/*hDevice != 3*/)
  {
    auto& fun_hook = s_hooks.find("GetRawInputDeviceInfo")->second;
    fun_hook.disable();

    auto orignal_fun = reinterpret_cast<decltype(&GetRawInputDeviceInfo)>(fun_hook.get_original());
    auto result = orignal_fun(hDevice, uiCommand, pData, pcbSize);

    fun_hook.enable();
    return result;
  }

  switch (uiCommand)
  {
  case RIDI::DeviceName :
    dout << "Plasium GetRawInputDeviceInfo devicename " << std::endl;
    if (pData && *pcbSize >= 15)
    {
      std::memcpy(pData, "Plasium device", 15*sizeof(char));
      return 15*sizeof(char);
    }
    else
    {
      *pcbSize = 15*sizeof(char) ;
      return (pData==nullptr)? 0 : -1;
    }
    break;

  case RIDI::DeviceInfo :
    {
      dout << "Plasium GetRawInputDeviceInfo deviceinfo " << std::endl;
      if (pData && *pcbSize >= sizeof(RID_DEVICE_INFO))
      {
        RID_DEVICE_INFO* devinfo = static_cast<RID_DEVICE_INFO*>(pData);
        devinfo->cbSize = sizeof(RID_DEVICE_INFO);
        devinfo->dwType =  static_cast<int>(RIMTYPE::HID);
        devinfo->hid.dwVendorId = 0x054c;
        devinfo->hid.dwProductId = 0x05c4;
        devinfo->hid.dwVersionNumber = 1;
        devinfo->hid.usUsagePage = 1;
        devinfo->hid.usUsage = 4; /* joystick */
        return *pcbSize;
      }
      else
      {
        *pcbSize = sizeof(RID_DEVICE_INFO);
        return (pData==nullptr)? 0 : -1;
      }
      break;

    case RIDI::PreparseData :
      if (pData && *pcbSize >= sizeof(uintptr_t))
      {
        dout << "Plasium GetRawInputDeviceInfo preparsedata" << std::endl;
        auto device_list = Manager::instance().get_devices();
        *static_cast<uintptr_t*>(pData) = reinterpret_cast<uintptr_t>(device_list[0]);
        dout << "Plasium GetRawInputDeviceInfo preparsedata end " << device_list[0] << std::endl;
        return sizeof(uintptr_t);
      }
      else
      {
        dout << "Plasium GetRawInputDeviceInfo preparsedata ask size" << std::endl;
        *pcbSize = sizeof(uintptr_t);
        return (pData==nullptr)? 0 : -1;
      }
      break;
    }
  };

  assert(false);
  return 0;
}

uint32_t WINAPI
U32_Input::GetRawInputData(RAWINPUT* hRawInput, uint32_t uiCommand,
                           void* pData, uint32_t* pcbSize, uint32_t cbSizeHeader)
{
  Service::instance().info() << "Plasium GetRawInputData : " << std::endl;
  /*
  auto& fun_hook = s_hooks.find("GetRawInputData")->second;
  fun_hook.disable();

  auto orignal_fun = reinterpret_cast<decltype(&GetRawInputData)>(fun_hook.get_original());
  auto result = orignal_fun(hRawInput, uiCommand, pData, pcbSize, cbSizeHeader);

  fun_hook.enable();
  */

  if (pData == nullptr)
  {
    *pcbSize = sizeof(RAWINPUT);
    return 0;
  }

  std::memcpy(pData, hRawInput, sizeof(RAWINPUT));

  return 0;
}

} /* namespace Plasium */
