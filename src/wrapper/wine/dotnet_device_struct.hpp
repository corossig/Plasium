/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DOTNET_DEVICE_STRUCT_HPP__
#define __DOTNET_DEVICE_STRUCT_HPP__

#include <cstdint>

namespace Plasium
{


struct VARIANT
{
  unsigned short vt;
  short          wReserved1;
  short          wReserved2;
  short          wReserved3;
  const wchar_t  *STR;
};


/* struct from WbemCli.h */

struct IWbemServicesVtbl
{
  uintptr_t QueryInterface; /* HRESULT (*)(IWbemServices* This, REFIID riid, void** ppvObject); */

  uintptr_t AddRef; /* ULONG (*)(IWbemServices* This); */

  uintptr_t Release; /* ULONG (*)(IWbemServices* This); */

  uintptr_t OpenNamespace;/*  HRESULT (*)(IWbemServices * This, const BSTR strNamespace, long lFlags, IWbemContext *pCtx, IWbemServices **ppWorkingNamespace, IWbemCallResult **ppResult); */

  uintptr_t CancelAsyncCall; /* HRESULT (*)(IWbemServices * This, IWbemObjectSink *pSink); */

  uintptr_t QueryObjectSink; /* HRESULT (*)(IWbemServices * This, long lFlags, IWbemObjectSink **ppResponseHandler); */

  uintptr_t GetObject; /* HRESULT (*)(IWbemServices * This, const BSTR strObjectPath, long lFlags, IWbemContext *pCtx, IWbemClassObject **ppObject, IWbemCallResult **ppCallResult); */

  uintptr_t GetObjectAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strObjectPath, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t PutClass; /* HRESULT (*)(IWbemServices * This, IWbemClassObject *pObject, long lFlags, IWbemContext *pCtx, IWbemCallResult **ppCallResult);*/

  uintptr_t PutClassAsync; /* HRESULT (*)(IWbemServices * This, IWbemClassObject *pObject, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t DeleteClass; /* HRESULT (*)(IWbemServices * This, const BSTR strClass, long lFlags, IWbemContext *pCtx, IWbemCallResult **ppCallResult); */

  uintptr_t DeleteClassAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strClass, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t CreateClassEnum; /* HRESULT (*)(IWbemServices * This, const BSTR strSuperclass, long lFlags, IWbemContext *pCtx, IEnumWbemClassObject **ppEnum); */

  uintptr_t CreateClassEnumAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strSuperclass, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t PutInstance; /* HRESULT (*)(IWbemServices * This, IWbemClassObject *pInst, long lFlags, IWbemContext *pCtx, IWbemCallResult **ppCallResult); */

  uintptr_t PutInstanceAsync; /* HRESULT (*)(IWbemServices * This, IWbemClassObject *pInst, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t DeleteInstance; /* HRESULT (*)(IWbemServices * This, const BSTR strObjectPath, long lFlags, IWbemContext *pCtx, IWbemCallResult **ppCallResult); */

  uintptr_t DeleteInstanceAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strObjectPath, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t CreateInstanceEnum; /* HRESULT (*)(IWbemServices * This, const BSTR strFilter, long lFlags, IWbemContext *pCtx, IEnumWbemClassObject **ppEnum); */

  uintptr_t CreateInstanceEnumAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strFilter, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t ExecQuery; /* HRESULT (*)(IWbemServices * This, const BSTR strQueryLanguage, const BSTR strQuery, long lFlags, IWbemContext *pCtx, IEnumWbemClassObject **ppEnum); */

  uintptr_t ExecQueryAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strQueryLanguage, const BSTR strQuery, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t ExecNotificationQuery; /* HRESULT (*)(IWbemServices * This, const BSTR strQueryLanguage, const BSTR strQuery, long lFlags, IWbemContext *pCtx, IEnumWbemClassObject **ppEnum); */

  uintptr_t ExecNotificationQueryAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strQueryLanguage, const BSTR strQuery, long lFlags, IWbemContext *pCtx, IWbemObjectSink *pResponseHandler); */

  uintptr_t ExecMethod; /* HRESULT (*)(IWbemServices * This, const BSTR strObjectPath, const BSTR strMethodName, long lFlags, IWbemContext *pCtx, IWbemClassObject *pInParams, IWbemClassObject **ppOutParams, IWbemCallResult **ppCallResult); */

  uintptr_t ExecMethodAsync; /* HRESULT (*)(IWbemServices * This, const BSTR strObjectPath, const BSTR strMethodName, long lFlags, IWbemContext *pCtx, IWbemClassObject *pInParams, IWbemObjectSink *pResponseHandler); */

}; /* struct IWbemServicesVtbl */




struct IEnumWbemClassObjectVtbl
{
  uintptr_t QueryInterface; /* HRESULT (*)(IEnumWbemClassObject * This, REFIID riid, void **ppvObject); */

  uintptr_t AddRef; /* ULONG (*)(IEnumWbemClassObject * This); */

  uintptr_t Release; /* ULONG (*)(IEnumWbemClassObject * This); */

  uintptr_t Reset; /* HRESULT (*)(IEnumWbemClassObject * This); */

  uintptr_t Next; /* HRESULT (*)(IEnumWbemClassObject * This, long lTimeout, ULONG uCount, IWbemClassObject **apObjects, ULONG *puReturned); */

  uintptr_t NextAsync; /* HRESULT (*)(IEnumWbemClassObject * This, ULONG uCount, IWbemObjectSink *pSink); */

  uintptr_t Clone; /* HRESULT (*)(IEnumWbemClassObject * This, IEnumWbemClassObject **ppEnum); */

  uintptr_t Skip; /* HRESULT (*)(IEnumWbemClassObject * This, long lTimeout, ULONG nCount); */

}; /* IEnumWbemClassObjectVtbl */



struct IWbemClassObjectVtbl
{
  uintptr_t QueryInterface; /* HRESULT (*)(IWbemClassObject * This, REFIID riid, void **ppvObject); */

  uintptr_t AddRef; /* ULONG (*)(IWbemClassObject * This); */

  uintptr_t Release; /* ULONG (*)(IWbemClassObject * This); */

  uintptr_t GetQualifierSet; /* HRESULT (*)(IWbemClassObject * This, IWbemQualifierSet **ppQualSet); */

  uintptr_t Get; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszName, long lFlags, VARIANT *pVal, CIMTYPE *pType, long *plFlavor); */

  uintptr_t Put; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszName, long lFlags, VARIANT *pVal, CIMTYPE Type); */

  uintptr_t Delete; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszName); */

  uintptr_t GetNames; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszQualifierName, long lFlags, VARIANT *pQualifierVal, SAFEARRAY * *pNames); */

  uintptr_t BeginEnumeration; /* HRESULT (*)(IWbemClassObject * This, long lEnumFlags); */

  uintptr_t Next; /* HRESULT (*)(IWbemClassObject * This, long lFlags, BSTR *strName, VARIANT *pVal, CIMTYPE *pType, long *plFlavor); */

  uintptr_t EndEnumeration; /* HRESULT (*)(IWbemClassObject * This); */

  uintptr_t GetPropertyQualifierSet; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszProperty, IWbemQualifierSet **ppQualSet); */

  uintptr_t Clone; /* HRESULT (*)(IWbemClassObject * This, IWbemClassObject **ppCopy); */

  uintptr_t GetObjectText; /* HRESULT (*)(IWbemClassObject * This, long lFlags, BSTR *pstrObjectText); */

  uintptr_t SpawnDerivedClass; /* HRESULT (*)(IWbemClassObject * This, long lFlags, IWbemClassObject **ppNewClass); */

  uintptr_t SpawnInstance; /* HRESULT (*)(IWbemClassObject * This, long lFlags, IWbemClassObject **ppNewInstance); */

  uintptr_t CompareTo; /* HRESULT (*)(IWbemClassObject * This, long lFlags, IWbemClassObject *pCompareTo); */

  uintptr_t GetPropertyOrigin; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszName, BSTR *pstrClassName); */

  uintptr_t InheritsFrom; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR strAncestor); */

  uintptr_t GetMethod; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszName, long lFlags, IWbemClassObject **ppInSignature, IWbemClassObject **ppOutSignature); */

  uintptr_t PutMethod; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszName, long lFlags, IWbemClassObject *pInSignature, IWbemClassObject *pOutSignature); */

  uintptr_t DeleteMethod; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszName); */

  uintptr_t BeginMethodEnumeration; /* HRESULT (*)(IWbemClassObject * This, long lEnumFlags); */

  uintptr_t NextMethod; /* HRESULT (*)(IWbemClassObject * This, long lFlags, BSTR *pstrName, IWbemClassObject **ppInSignature, IWbemClassObject **ppOutSignature); */

  uintptr_t EndMethodEnumeration; /* HRESULT (*)(IWbemClassObject * This); */

  uintptr_t GetMethodQualifierSet; /* HRESULT (*)(IWbemClassObject * This, LPCWSTR wszMethod, IWbemQualifierSet **ppQualSet); */

  uintptr_t GetMethodOrigin; /*  HRESULT (*)(IWbemClassObject * This, LPCWSTR wszMethodName, BSTR *pstrClassName); */

}; /* struct IWbemClassObjectVtbl */



struct IDirectInput8AVtbl
{
  uintptr_t QueryInterface; /* HRESULT (*)(IWbemClassObject * This, REFIID riid, void **ppvObject); */

  uintptr_t AddRef; /* ULONG (*)(IWbemClassObject * This); */

  uintptr_t Release; /* ULONG (*)(IWbemClassObject * This); */

  uintptr_t CreateDevice; /* (*)(THIS_ REFGUID,LPDIRECTINPUTDEVICE8A *,LPUNKNOWN) PURE; */

  uintptr_t EnumDevices; /* (*)(THIS_ DWORD,LPDIENUMDEVICESCALLBACKA,LPVOID,DWORD) PURE; */

  uintptr_t GetDeviceStatus; /* (*)(THIS_ REFGUID) PURE; */

  uintptr_t RunControlPanel; /* (*)(THIS_ HWND,DWORD) PURE; */

  uintptr_t Initialize; /* (*)(THIS_ HINSTANCE,DWORD) PURE; */

  uintptr_t FindDevice; /* (*)(THIS_ REFGUID,LPCSTR,LPGUID) PURE; */

  uintptr_t EnumDevicesBySemantics; /* (*)(THIS_ LPCSTR,LPDIACTIONFORMATA,LPDIENUMDEVICESBYSEMANTICSCBA,LPVOID,DWORD) PURE; */

  uintptr_t ConfigureDevices; /* (*)(THIS_ LPDICONFIGUREDEVICESCALLBACK,LPDICONFIGUREDEVICESPARAMSA,DWORD,LPVOID) PURE; */

}; /* struct IDirectInput8AVtbl */

} /* namespace Plasium */

#endif /* __DOTNET_DEVICE_STRUCT_HPP__ */
