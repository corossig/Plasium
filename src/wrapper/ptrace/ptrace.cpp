/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/ptrace/ptrace.hpp>

#include <service.hpp>

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/wait.h>
#include <signal.h>
#include <string>
#include <cstdint>

namespace Plasium
{

} /* namespace Plasium */


int
wait_for_syscall(pid_t child)
{
  int status;
  while (true)
  {
    ptrace(PTRACE_SYSCALL, child, 0, 0);
    waitpid(child, &status, 0);
    if (WIFSTOPPED(status) && WSTOPSIG(status) & 0x80)
      return 0;
    if (WIFEXITED(status))
      return 1;
  }
}


std::string
read_string(int child, uintptr_t addr, long count=-1)
{
  std::string str;

  // Read string 1 long at a time
  int read = 0;
  while ( count != 0 )
  {
    char tmp[sizeof(long)];

    *(long*)tmp = ptrace(PTRACE_PEEKDATA, child, addr + read);
    if (errno != 0)
      break;

    for (size_t i = 0; i < sizeof(long); ++i)
    {
      if (count == -1 && tmp[i] == '\0')
        return str;
      str += tmp[i];
      read++;

      if (count != -1 && --count == 0)
        return str;
    }
  }

  return str;
}


void
write_string(int child, uintptr_t addr, const std::string& str)
{
  // write string 1 long at a time
  for (size_t i = 0; i < str.length(); i+=sizeof(long))
  {
    long tmp = *(long*)(str.c_str()+i);

    ptrace(PTRACE_POKEDATA, child, addr+i, tmp);
    if (errno != 0)
      break;
  }
}


struct IOC
{
  char     nr:8;
  char     type:8;
  uint16_t size:13;
  char     dir:3;
};
static_assert(sizeof(IOC) == 4, "IOC must be 4 Bytes");


int
main(int argc, const char *argv[])
{
  if (argc < 2)
  {
    std::cerr << "Usage: " << argv[0] << " args" << std::endl;
    std::exit(1);
  }

  pid_t child = ::fork();
  if (child == 0)
  {
    char** args = static_cast<char**>(malloc(argc*sizeof(char*)));
    std::memcpy(args, argv+1, (argc-1)*sizeof(char*));
    args[argc] = NULL;

    ptrace(PTRACE_TRACEME);
    kill(getpid(), SIGSTOP);
    execvp(args[0], args);

    // never reach
    std::exit(1);
  }
  else
  {
    int status, syscall;
    waitpid(child, &status, 0);

    ptrace(PTRACE_SETOPTIONS, child, 0, PTRACE_O_TRACESYSGOOD);
    while (true)
    {
        if (wait_for_syscall(child) != 0) break; // Before
        if (wait_for_syscall(child) != 0) break; // After
        syscall = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*ORIG_RAX);
        if (syscall == 2)
        {
          uintptr_t filename_ptr;
          long flag;
          long mode;
          filename_ptr = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RDI);
          flag         = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RSI);
          mode         = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RDX);
          std::string filename = read_string(child, filename_ptr);

          //Plasium::Service::instance().info() << "open(" << filename << ", " << flag <<", " << mode << ")\n";
        }
        else if(syscall == 1) // Write
        {
          long fd            = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RDI);
          uintptr_t data_ptr = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RSI);
          long count         = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RDX);
          std::string data   = read_string(child, data_ptr, count);

          //Plasium::Service::instance().info() << "write(" << fd << ", " << data << ", " << count << ")\n";
        }
        else if(syscall == 16) // Ioctl
        {
          long fd  = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RDI);
          long cmd = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RSI);
          long arg = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RDX);
          IOC cmd_ioc = *(IOC*)&cmd;

          //_IOC(/*dir*/_IOC_READ, /*type*/'j', /*nr*/0x13, /*size*/len) get string identifier
          if (cmd_ioc.type == 'j' && cmd_ioc.nr == 0x13)
          {
            // Name all Js device Plasium device
            std::string dev_name("Plasium device");
            write_string(child, arg, dev_name);
            Plasium::Service::instance().info() << "ioctl(" << fd << ", " << cmd_ioc.type << ", " << arg << ")\n";
          }
        }
    }
  }

  return 0;
}
