/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <wrapper/ptrace/js_provider.hpp>

#include <wrapper/ptrace/ptrace.hpp>
#include <device.hpp>

#include <sys/types.h>
#include <sys/stat.h>

namespace Plasium
{

JS_Provider::JS_Provider(Ptrace* ptrace_obj, Device* device)
  : m_ptrace(ptrace_obj)
  , m_device(device)
{
}


JS_Provider::~JS_Provider()
{
}


std::vector<std::string>
JS_Provider::get_filter()
{
  return std::vector<std::string> {
    {"/dev/js*"},
    {"/dev/input/js*"},
    {"/dev/input/by-id/*-joystick"},
    {"/dev/input/by-path/*-joystick"},
    {"/sys/class/input/js*"}
  };
}

} /* namespace Plasium */
