/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __WRAPPER_PTRACE_JS_PROVIDER_HPP__
#define __WRAPPER_PTRACE_JS_PROVIDER_HPP__

#include <vector>
#include <string>

namespace Plasium
{
class Device;
class Ptrace;

/*
 * This class emulate Joystick API of Linux
 */

class JS_Provider
{
public :
  JS_Provider(Ptrace* ptrace_obj, Device* device);
  ~JS_Provider();

  // List of glob expressions to remove from the filesystem
  static std::vector<std::string> get_filter();

private :
  Ptrace*     m_ptrace;
  Device*     m_device;
  std::string m_fifo_path;
  int         m_fifo;
};


} /* namespace Plasium */

#endif /* __WRAPPER_PTRACE_JS_PROVIDER_HPP__ */
