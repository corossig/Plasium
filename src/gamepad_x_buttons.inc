X(bool, leftcross_up,    LEFTCROSS_UP,    LC_U)    // top button on the left side of the gamepad
X(bool, leftcross_down,  LEFTCROSS_DOWN,  LC_D)  // bottom button on the left side of the gamepad
X(bool, leftcross_left,  LEFTCROSS_LEFT,  LC_L)  // left button on the left side of the gamepad
X(bool, leftcross_right, LEFTCROSS_RIGHT, LC_R) // right button on the left side of the gamepad


X(bool, rightcross_left,  RIGHTCROSS_LEFT,  RC_L)  // left button on the right side of the gamepad
X(bool, rightcross_right, RIGHTCROSS_RIGHT, RC_R) // right button on the right side of the gamepad

X(bool, select, SELECT, B_Select) // left button in the center
X(bool, start,  START,  B_Start)  // right button in the center

X(bool, rightcross_up,   RIGHTCROSS_UP,   RC_U)   // top button on the right side of the gamepad
X(bool, rightcross_down, RIGHTCROSS_DOWN, RC_D) // bottom button on the right side of the gamepad

X(bool, L1, L1, B_L1) // top-left button on the back
X(bool, R1, R1, B_R1) // top-right button on the back

X(bool, L2, L2, B_L2) // bottom-left button on the back
X(bool, R2, R2, B_R2) // bottom-right button on the back

X(bool, L3, L3, B_L3) // button on the left joystick
X(bool, R3, R3, B_R3) // button on the right joystick

X(bool, super, SUPER, B_Super) // middle button in the center of the gamepad
