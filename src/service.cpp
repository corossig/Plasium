/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <service.hpp>

#include <device.hpp>

#include <iostream>


namespace
{
class Empty_ostream : public std::ostream
{
public :
  Empty_ostream() {}
};


class prefix_streambuf : public std::streambuf
{
public:
  prefix_streambuf(const std::string& prefix, std::streambuf* stream)
    : m_prefix(prefix)
    , m_stream(stream)
    , m_print_prefix(true) //< true for the first line
  {
  }

  void
  set_prefix(const std::string& prefix)
  {
    m_prefix = prefix;
  }

private :
  int
  sync() override
  {
    return m_stream->pubsync();
  }

  int
  overflow(int c) override
  {
    if (c != std::char_traits<char>::eof())
    {
      if (m_print_prefix && !m_prefix.empty())
      {
        if (m_stream->sputn(m_prefix.c_str(), m_prefix.size())
            != static_cast<ssize_t>(m_prefix.size()))
          return std::char_traits<char>::eof();
      }
    }

    m_print_prefix = (c == '\n');
    return m_stream->sputc(c);
  }

private :
  std::string     m_prefix;
  std::streambuf* m_stream;
  bool            m_print_prefix;
};


class Prefix_ostream : private virtual prefix_streambuf, public std::ostream
{
public:
  Prefix_ostream(const std::string& prefix, std::ostream& out)
    : prefix_streambuf(prefix, out.rdbuf())
    , std::ios(static_cast<std::streambuf*>(this))
    , std::ostream(static_cast<std::streambuf*>(this))
  {
  }

  void
  set_prefix(const std::string& prefix)
  {
    prefix_streambuf::set_prefix(prefix);
  }
};

} /* anonymous namespace */


namespace Plasium
{

std::unique_ptr<Service> Service::s_instance;


Service&
Service::instance()
{
  if (!s_instance)
    s_instance.reset(new Service());

  return *s_instance;
}



Service::Service()
  : m_debug_level(1)
  , m_prefix_msg("Plasium : ")
  , m_empty_ostream(new Empty_ostream())
  , m_cout_ostream(new Prefix_ostream(m_prefix_msg, std::cout))
  , m_cerr_ostream(new Prefix_ostream(m_prefix_msg, std::cerr))
{
  if (::getenv("PLASIUM_DEBUG"))
  {
    m_debug_level = ::atoi(::getenv("PLASIUM_DEBUG"));
  }
}


Service::~Service()
{
}


void
Service::info(const std::string& message)
{
  info() << message << std::endl;
}

std::ostream&
Service::info()
{
  if (m_debug_level >= 3)
    return *m_cout_ostream;
  else
    return *m_empty_ostream;
}


void
Service::warning(const std::string& message)
{
  warning() << message << std::endl;
}

std::ostream&
Service::warning()
{
  if (m_debug_level >= 2)
    return *m_cerr_ostream;
  else
    return *m_empty_ostream;
}


void
Service::error(const std::string& message)
{
  error() << message << std::endl;
}

std::ostream&
Service::error()
{
  if (m_debug_level >= 1)
    return *m_cerr_ostream;
  else
    return *m_empty_ostream;
}


Input_Driver
Service::get_input_driver()
{
  if (::getenv("PLASIUM_INPUT"))
  {
    std::string input_str(::getenv("PLASIUM_INPUT"));
    if (!input_str.empty())
    {
      if (input_str == "SDL")
        return Input_Driver::SDL;

      if (input_str == "Js")
        return Input_Driver::Js;
    }
  }

  return Input_Driver::Evdev;
}


} /* namespace Plasium */
