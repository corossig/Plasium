/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DEVICE_HPP__
#define __DEVICE_HPP__

#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include <gamepad.hpp>

/*
 * This class describe a physical device
 */

namespace Plasium
{
class Mapping;

enum class Input_Driver
{
  Evdev,
  SDL,
  Js
};


class Device
{
public :
  Device();
  virtual ~Device();

  const std::string& get_name() const;

  int max_level_buttons() const; ///< what are the buttons supported by the device
  int max_level_axis() const; ///< what are the axis supported by the device

  virtual bool assign_buttons(Buttons_Enum, bool*) { return false; }

  void save() const;

  virtual bool save_mapping(std::ostream& s) const = 0;
  virtual bool load_mapping(std::istream& s) = 0;

  virtual const Gamepad_State& get_state() = 0;

  const std::vector<bool>& get_raw_buttons_state();

protected :
  static std::string ids_to_string(uint16_t id);

protected :
  std::string m_name;
  uint16_t    m_vendor_id;
  uint16_t    m_product_id;
  std::unique_ptr<Mapping> m_mapping;

  std::vector<bool> m_raw_buttons_state;

  int m_level_buttons;
  int m_level_axis;

  Gamepad_State m_state;
};

} /* end namespace Plasium */

#endif /* __DEVICE_HPP__ */
