;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c++-mode
  (auto-save-interval . 5)
  (c-file-style . "gnu")
  (indent-tabs-mode)
  (eval . (progn
	    (c-set-offset 'innamespace '0)
            (c-set-offset 'inline-open '0)
            (c-set-offset 'substatement-open 0)))
  (eval . (setq ff-search-directories (list "." (locate-dominating-file buffer-file-name ".dir-locals.el"))
                ))))
                 

(provide '.dir-locals)
; .dir-locals.el ends here'((locate-dominating-file buffer-file-name ".dir-locals.el"))
