/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __MANAGER_HPP__
#define __MANAGER_HPP__

#include <memory>
#include <vector>

namespace Plasium
{
class Device;
enum class Input_Driver;

class Manager
{
public :
  static Manager& instance();
  ~Manager();

  std::vector<Device*> get_devices();

private :
  Manager(Input_Driver driver);

private :
  static std::unique_ptr<Manager> s_instance;

  std::vector<std::unique_ptr<Device>> m_devices;
};


} /* namespace Plasium */

#endif /* __MANAGER_HPP__ */
