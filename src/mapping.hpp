/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __MAPPING_HPP__
#define __MAPPING_HPP__

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <iostream>

namespace Plasium
{
enum class Buttons_Enum;
enum class Axis_Enum;
struct Hat_Button
{
  int  id;
  bool positive;
};


class Mapping
{
public :
  /**
   * Create a mapping and try to load mapping for specific (vendor_id, product_id)
   */
  Mapping(const std::string& vendor_id, const std::string& product_id);
  Mapping(const Mapping& obj);
  ~Mapping();

  void load_file(const std::string& filename);

  /**
   * Save mapping inside filename. If filename is nullptr, use default config dir.
   */
  void save_file(const std::string* filename = nullptr) const;


  void load_stream(std::istream& s);
  void save_stream(std::ostream& s) const;


  const std::vector<Buttons_Enum>& get_buttons_mapping() const;
  const std::vector<Axis_Enum>&    get_axis_mapping() const;
  const std::map<Buttons_Enum, Hat_Button>& get_hat_mapping() const;

  void set_button(int i, Buttons_Enum b);
  void set_axis(int i, Axis_Enum a);
  void set_hat(Hat_Button hat, Buttons_Enum b);

  uint16_t get_vendor_id() const;
  uint16_t get_product_id() const;

private :
  Mapping(const std::string& filename);
  Mapping& operator=(const Mapping& source) =default;


  static std::string get_mapping_dir();
  static void        parse_config();

private :
  std::string m_vendor_id;
  std::string m_product_id;

  std::vector<Buttons_Enum>           m_buttons;
  std::vector<Axis_Enum>              m_axis;
  std::map<Buttons_Enum, Hat_Button>  m_hat;

  static std::map<std::string, std::unique_ptr<Mapping>> s_mappings;
};


} /* namespace Plasium */

#endif /* __MAPPING_HPP__ */
