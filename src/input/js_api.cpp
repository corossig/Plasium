/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <input/js_api.hpp>

#include <mapping.hpp>
#include <service.hpp>

#include <vector>
#include <string>
#include <cstring>
#include <cstdint>
#include <fstream>

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <stropts.h>
#include <linux/joystick.h>
#include <unistd.h>
#include <linux/hiddev.h>
#include <libevdev-1.0/libevdev/libevdev.h>


namespace Plasium
{

std::vector<std::unique_ptr<Device>>
Joystick_API::get_devices()
{
  std::vector<std::unique_ptr<Device>> list_devices;

  std::string dev_path("/dev/input/");
  DIR* dev_dir = opendir(dev_path.c_str());
  if (dev_dir)
  {
    struct dirent *ep;
    while ( (ep = readdir(dev_dir)) )
    {
      if (ep->d_type == DT_CHR)
      {
        try
        {
          if (strncmp(ep->d_name, "js", 2) == 0)
            list_devices.emplace_back(new Joystick_API(dev_path + ep->d_name));
        }
        catch (const std::runtime_error& e)
        {
          ;
        }
      }
    }
    closedir(dev_dir);
  }

  return list_devices;
}

Joystick_API::Joystick_API(std::string filepath)
{
  m_fd = open(filepath.c_str(), O_RDONLY|O_NONBLOCK);

  char name[128];
  if (ioctl(m_fd, JSIOCGNAME(sizeof(name)), name) < 0)
    strncpy(name, "Unknown", sizeof(name));
  printf("Name: %s\n", name);
  m_name = name;

  uint8_t number_of_axes;
  ioctl(m_fd, JSIOCGAXES, &number_of_axes);
  printf("Axes: %d\n", (int)number_of_axes);

  uint8_t number_of_buttons;
  ioctl(m_fd, JSIOCGBUTTONS, &number_of_buttons);
  printf("Buttons: %d\n", (int)number_of_buttons);

  uint32_t version;
  ioctl(m_fd, JSIOCGVERSION, &version);
  printf("Version: %d\n", version);

  printf("-----------------\n");

  std::string vendor_id_str;
  std::string product_id_str;

  auto js_pos = filepath.rfind("js");
  if (js_pos != std::string::npos)
  {
    std::string vendor_path  = "/sys/class/input/" + filepath.substr(js_pos) + "/device/id/vendor";
    std::string product_path = "/sys/class/input/" + filepath.substr(js_pos) + "/device/id/product";

    std::ifstream vendor_stream(vendor_path);
    if (vendor_stream.is_open())
    {
      vendor_stream >> vendor_id_str;
      vendor_stream.seekg(0);
      vendor_stream >> std::hex >> m_vendor_id;
    }

    std::ifstream product_stream(product_path);
    if (product_stream.is_open())
    {
      product_stream >> product_id_str;
      product_stream.seekg(0);
      product_stream >> std::hex >> m_product_id;
    }
  }

  m_mapping.reset(new Mapping(vendor_id_str, product_id_str));
}


Joystick_API::~Joystick_API()
{
  m_mapping->save_file(); // Temporary : save mapping each time
  close(m_fd);
}


bool
Joystick_API::assign_buttons(Buttons_Enum b, bool* must_stop)
{
  struct js_event events[50];

  while (! *must_stop)
  {
    // Ask for maximum 50 events before returning the state
    int nb_event = read(m_fd, events, 50*sizeof(struct js_event));

    if (nb_event > 0)
    {
      nb_event /= sizeof(struct js_event);
      for (int i = 0; i < nb_event; ++i)
      {
        struct js_event& event = events[i];

        if (event.type == JS_EVENT_BUTTON && event.value == 1)
        {
          m_mapping->set_button(event.number, b);
          return true;
        }
        else if (event.type == JS_EVENT_AXIS && event.value != 0)
        {
          // Why 6? number of axis ? Need another gamepad to test this value
          if (event.number >= 6 && event.number < 10)
          {
            Hat_Button hat;
            hat.id       = event.number-6;
            hat.positive = event.value > 0;

            m_mapping->set_hat(hat, b);
            return true;
          }
        }
      }
    }
  }

  return false;
}


bool
Joystick_API::save_mapping(std::ostream& s) const
{
  (void) s;
  return false;
}


bool
Joystick_API::load_mapping(std::istream& s)
{
  (void) s;
  return false;
}

#if 0
struct js_event
{
   uint32_t time;   /* event timestamp in milliseconds */
   int16_t  value;  /* value */
   uint8_t  type;   /* event type */
   uint8_t  number; /* axis/button number */
};
#endif

const Gamepad_State&
Joystick_API::get_state()
{
  const auto& button_mapping = m_mapping->get_buttons_mapping();
  const auto& axis_mapping = m_mapping->get_axis_mapping();
  const auto& hat_mapping = m_mapping->get_hat_mapping();

  struct js_event events[50];
  /* TODO : fill m_state */

  // Ask for maximum 50 events before returning the state
  int nb_event = read(m_fd, events, 50*sizeof(struct js_event));

  if (nb_event < 0)
  {
    return m_state;
  }


  nb_event /= sizeof(struct js_event);
  for (int i = 0; i < nb_event; ++i)
  {
    struct js_event& event = events[i];

    if (event.type == JS_EVENT_BUTTON)
    {
      m_state.buttons[button_mapping[event.number]] = (event.value==1);
    }

    if (event.type == JS_EVENT_AXIS)
    {
      for (const auto& mapping : hat_mapping)
      {
        const Hat_Button& hat = mapping.second;
        // Why 6? number of axis ? Need another gamepad to test this value
        if (event.number == hat.id+6)
        {
          m_state.buttons[mapping.first] = (hat.positive) ? event.value > 0 : event.value < 0;
        }
      }

      if (event.number >= 6)
        continue;

      if (event.number > axis_mapping.size())
        continue;

      Axis_Enum axis = axis_mapping[event.number];
      m_state.axis[axis] = event.value;
    }
  }

  m_state.buttons[Buttons_Enum::L2] = (m_state.axis[Axis_Enum::L2] > 1);
  m_state.buttons[Buttons_Enum::R2] = (m_state.axis[Axis_Enum::R2] > 1);

  return m_state;
}

} /* namespace Plasium */
