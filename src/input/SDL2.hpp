/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <device.hpp>

#include <SDL2/SDL.h>
#include <vector>
#include <memory>

namespace Plasium
{

class SDL_Device : public Device
{
public :
  SDL_Device(int sdl_js_id);
  virtual ~SDL_Device();

  bool save_mapping(std::ostream& s) const override;
  bool load_mapping(std::istream& s) override;

  const Gamepad_State& get_state() override;

  static std::vector<std::unique_ptr<Device>> get_devices();

private :
  static void init_SDL();
  static void fini_SDL();

  bool get_button(SDL_GameControllerButton button);

private :
  SDL_Joystick*       m_joystick;
  SDL_GameController* m_controller;

  static int s_num_sdl_devices;
};

} /* namespace Plasium */
