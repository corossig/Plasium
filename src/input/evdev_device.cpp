/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <input/evdev_device.hpp>

#include <mapping.hpp>
#include <service.hpp>

#include <vector>
#include <algorithm>
#include <string>
#include <cstring>
#include <cstdint>

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <stropts.h>
#include <linux/joystick.h>
#include <unistd.h>
#include <linux/hiddev.h>
#include <libevdev-1.0/libevdev/libevdev.h>


namespace Plasium
{

std::vector<std::unique_ptr<Device>>
EVDev_Device::get_devices()
{
  std::vector<std::unique_ptr<Device>> list_devices;

  std::string dev_path("/dev/input/by-id/");
  DIR* dev_dir = opendir(dev_path.c_str());
  if (dev_dir)
  {
    struct dirent *ep;
    while ( (ep = readdir(dev_dir)) )
    {
      // if (ep->d_type == DT_CHR)
      {
        std::string device_fullpath(dev_path + ep->d_name);

        try
        {
          if (device_fullpath.find("event-joystick") !=std::string::npos)
            list_devices.emplace_back(new EVDev_Device( device_fullpath ));
        }
        catch (const std::runtime_error& e)
        {
          Service::instance().error() << "EVdev : " << e.what() << std::endl;
        }
      }
    }
    closedir(dev_dir);
  }

  return list_devices;
}

EVDev_Device::EVDev_Device(std::string filepath)
{
  m_fd = open(filepath.c_str(), O_RDONLY|O_NONBLOCK);
  if (m_fd == -1)
    throw std::runtime_error("Can't open device");

  if (libevdev_new_from_fd(m_fd, &m_dev) < 0)
    throw std::runtime_error("Can't open device with evdev");

  m_name       = libevdev_get_name(m_dev);
  m_vendor_id  = libevdev_get_id_vendor(m_dev);
  m_product_id = libevdev_get_id_product(m_dev);


  // Get the mapping for the current device
  auto vendor_id_str  = ids_to_string(m_vendor_id);
  auto product_id_str = ids_to_string(m_product_id);
  m_mapping.reset(new Mapping(vendor_id_str, product_id_str));


  // Reconize as joystick or gamepad
  if (libevdev_has_event_code(m_dev, EV_KEY, BTN_GAMEPAD))
    m_first_button = BTN_GAMEPAD;
  else
    m_first_button = BTN_JOYSTICK;

  // Build the list of available buttons
  for (int i = 0; i < 255; ++i)
  {
    if (libevdev_has_event_code(m_dev, EV_KEY, m_first_button+i))
      m_buttons_list.push_back(m_first_button+i);
  }


  for (int i = 0; i < 255; ++i)
  {
    if (libevdev_has_event_code(m_dev, EV_ABS, ABS_X+i))
      m_axis_list.push_back(ABS_X+i);
  }

  // Don't track unwanted events
  libevdev_disable_event_type(m_dev, EV_REL);
  libevdev_disable_event_type(m_dev, EV_SYN);
  libevdev_disable_event_type(m_dev, EV_FF);
  libevdev_disable_event_type(m_dev, EV_FF_STATUS);
  libevdev_disable_event_type(m_dev, EV_SND);
  libevdev_disable_event_type(m_dev, EV_CNT);
  libevdev_disable_event_type(m_dev, EV_PWR);
  libevdev_disable_event_type(m_dev, EV_SW);
  libevdev_disable_event_type(m_dev, EV_MSC);
  libevdev_disable_event_type(m_dev, EV_LED);
  libevdev_disable_event_type(m_dev, EV_MAX);
  libevdev_disable_event_type(m_dev, EV_REP);
}


EVDev_Device::~EVDev_Device()
{
  libevdev_free(m_dev);
  close(m_fd);
}


bool
EVDev_Device::save_mapping(std::ostream& s) const
{
  (void) s;
  return false;
}


bool
EVDev_Device::load_mapping(std::istream& s)
{
  (void) s;
  return false;
}


bool
EVDev_Device::assign_buttons(Buttons_Enum b, bool* must_stop)
{
  struct input_event event;

  while (! *must_stop)
  {
    if (libevdev_next_event(m_dev, LIBEVDEV_READ_FLAG_NORMAL, &event) == LIBEVDEV_READ_STATUS_SUCCESS)
    {
      if (libevdev_event_is_type(&event, EV_KEY) && event.value == 1)
      {
        int button_offset = std::find(m_buttons_list.begin(), m_buttons_list.end(), event.code) - m_buttons_list.begin();
        m_mapping->set_button(button_offset, b);

        return true;
      }
      else if (libevdev_event_is_type(&event, EV_ABS) && event.value != 0)
      {
        if (event.code >= ABS_HAT0X && event.code <= ABS_HAT3Y)
        {
          Hat_Button hat;
          hat.id       = event.code-ABS_HAT0X;
          hat.positive = event.value > 0;

          m_mapping->set_hat(hat, b);
          return true;
        }
      }
    }
  }

  return false;
}


const Gamepad_State&
EVDev_Device::get_state()
{
  const auto& button_mapping = m_mapping->get_buttons_mapping();
  const auto& axis_mapping = m_mapping->get_axis_mapping();
  const auto& hat_mapping = m_mapping->get_hat_mapping();

  struct input_event event;

  m_state.buttons.clear();
  m_state.axis.clear();

  for (int i = 0; i < 100; ++i)
  {
    libevdev_next_event(m_dev, LIBEVDEV_READ_FLAG_NORMAL, &event);
  }

  for (size_t i = 0; i < button_mapping.size(); ++i)
  {
    if (button_mapping[i] != Buttons_Enum::None && i < m_buttons_list.size())
    m_state.buttons[button_mapping[i]] = libevdev_get_event_value(m_dev, EV_KEY, m_buttons_list[i]) != 0;
  }

  for (const auto& mapping : hat_mapping)
  {
    const Hat_Button& hat = mapping.second;
    int event_value = libevdev_get_event_value(m_dev, EV_ABS, ABS_HAT0X+hat.id);
    m_state.buttons[mapping.first] = (hat.positive) ? event_value > 0 : event_value < 0;
  }

  for (size_t i = 0; i < axis_mapping.size(); ++i)
  {
    if (axis_mapping[i] != Axis_Enum::None)
    {
      int min = libevdev_get_abs_minimum(m_dev, m_axis_list[i]);
      int max = libevdev_get_abs_maximum(m_dev, m_axis_list[i]);
      uint64_t length = max - min;

      double value = libevdev_get_event_value(m_dev, EV_ABS, m_axis_list[i]) - min;
      value /= length;
      value *= std::numeric_limits<uint16_t>::max();
      value += std::numeric_limits<int16_t>::min();

      m_state.axis[axis_mapping[i]] = (int16_t)value;
    }
  }

  if ( ! m_state.buttons[Buttons_Enum::L2])
    m_state.buttons[Buttons_Enum::L2] = (m_state.axis[Axis_Enum::L2] > 1);

  if ( ! m_state.buttons[Buttons_Enum::R2])
    m_state.buttons[Buttons_Enum::R2] = (m_state.axis[Axis_Enum::R2] > 1);

  return m_state;
}

} /* namespace Plasium */
