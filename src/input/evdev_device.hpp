/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __EVDEV_DEVICE_HPP__
#define __EVDEV_DEVICE_HPP__

#include <device.hpp>

#include <vector>
#include <memory>

struct libevdev;

namespace Plasium
{

class EVDev_Device : public Device
{
public :
  EVDev_Device(std::string filepath);
  virtual ~EVDev_Device();

  bool save_mapping(std::ostream& s) const override;
  bool load_mapping(std::istream& s) override;

  bool assign_buttons(Buttons_Enum b, bool* must_stop) override;

  const Gamepad_State& get_state() override;

  static std::vector<std::unique_ptr<Device>> get_devices();

private :
  int m_fd; //< File descriptor on /dev/input/js
  struct libevdev *m_dev;
  int m_first_button;
  std::vector<int> m_buttons_list;
  std::vector<int> m_axis_list;
};

} /* namespace Plasium */

#endif /* __EVDEV_DEVICE_HPP__ */
