/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <input/SDL2.hpp>

#include <stdexcept>

#include <service.hpp>

namespace Plasium
{

int SDL_Device::s_num_sdl_devices = 0;

SDL_Device::SDL_Device(int sdl_js_id)
{
  init_SDL();

  m_joystick = SDL_JoystickOpen(sdl_js_id);
  if (m_joystick == nullptr)
    throw std::runtime_error(std::string("Can't find SDL Joystick"));

  m_controller = SDL_GameControllerOpen(sdl_js_id);
  if (m_controller == nullptr)
    throw std::runtime_error(std::string("Can't find SDL controller"));


  // Get vendor and product ids
  SDL_JoystickGUID guid = SDL_JoystickGetGUID(m_joystick);
  m_vendor_id  = guid.data[4] + (guid.data[5]<<8);
  m_product_id = guid.data[8] + (guid.data[9]<<8);

  m_name = SDL_JoystickName(m_joystick);
}


SDL_Device::~SDL_Device()
{
  SDL_JoystickClose(m_joystick);
  SDL_GameControllerClose(m_controller);
  fini_SDL();
}


bool
SDL_Device::save_mapping(std::ostream& s) const
{
  (void) s;
  return false;
}


bool
SDL_Device::load_mapping(std::istream& s)
{
  (void) s;
  return false;
}


bool
SDL_Device::get_button(SDL_GameControllerButton button)
{
  return (SDL_GameControllerGetButton(m_controller, button) == 1);
}


const Gamepad_State&
SDL_Device::get_state()
{
  SDL_JoystickUpdate();
  SDL_GameControllerUpdate();


  m_state.buttons[Buttons_Enum::LEFTCROSS_UP]     = get_button(SDL_CONTROLLER_BUTTON_DPAD_UP);
  m_state.buttons[Buttons_Enum::LEFTCROSS_DOWN]   = get_button(SDL_CONTROLLER_BUTTON_DPAD_DOWN);
  m_state.buttons[Buttons_Enum::LEFTCROSS_LEFT]   = get_button(SDL_CONTROLLER_BUTTON_DPAD_LEFT);
  m_state.buttons[Buttons_Enum::LEFTCROSS_RIGHT]  = get_button(SDL_CONTROLLER_BUTTON_DPAD_RIGHT);

  m_state.buttons[Buttons_Enum::RIGHTCROSS_LEFT]  = get_button(SDL_CONTROLLER_BUTTON_X);
  m_state.buttons[Buttons_Enum::RIGHTCROSS_RIGHT] = get_button(SDL_CONTROLLER_BUTTON_B);

  m_state.buttons[Buttons_Enum::SELECT]           = get_button(SDL_CONTROLLER_BUTTON_BACK);
  m_state.buttons[Buttons_Enum::START]            = get_button(SDL_CONTROLLER_BUTTON_START);

  m_state.buttons[Buttons_Enum::RIGHTCROSS_UP]    = get_button(SDL_CONTROLLER_BUTTON_Y);
  m_state.buttons[Buttons_Enum::RIGHTCROSS_DOWN]  = get_button(SDL_CONTROLLER_BUTTON_A);

  m_state.buttons[Buttons_Enum::L1]               = get_button(SDL_CONTROLLER_BUTTON_LEFTSHOULDER);
  m_state.buttons[Buttons_Enum::R1]               = get_button(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER);

  m_state.axis[Axis_Enum::LEFT_X]  = SDL_GameControllerGetAxis(m_controller, SDL_CONTROLLER_AXIS_LEFTX);
  m_state.axis[Axis_Enum::LEFT_Y]  = SDL_GameControllerGetAxis(m_controller, SDL_CONTROLLER_AXIS_LEFTY);
  m_state.axis[Axis_Enum::RIGHT_X] = SDL_GameControllerGetAxis(m_controller, SDL_CONTROLLER_AXIS_RIGHTX);
  m_state.axis[Axis_Enum::RIGHT_Y] = SDL_GameControllerGetAxis(m_controller, SDL_CONTROLLER_AXIS_RIGHTY);

  m_state.buttons[Buttons_Enum::L3]               = get_button(SDL_CONTROLLER_BUTTON_LEFTSTICK);
  m_state.buttons[Buttons_Enum::R3]               = get_button(SDL_CONTROLLER_BUTTON_RIGHTSTICK);

  m_state.buttons[Buttons_Enum::SUPER]            = get_button(SDL_CONTROLLER_BUTTON_GUIDE);

  m_state.axis[Axis_Enum::L2] = SDL_GameControllerGetAxis(m_controller, SDL_CONTROLLER_AXIS_TRIGGERLEFT);
  m_state.axis[Axis_Enum::R2] = SDL_GameControllerGetAxis(m_controller, SDL_CONTROLLER_AXIS_TRIGGERRIGHT);

  m_state.buttons[Buttons_Enum::L2] = m_state.axis[Axis_Enum::L2] > 1;
  m_state.buttons[Buttons_Enum::R2] = m_state.axis[Axis_Enum::R2] > 1;

  return m_state;
}


std::vector<std::unique_ptr<Device>>
SDL_Device::get_devices()
{
  std::vector<std::unique_ptr<Device>> devices;

  init_SDL();
  for(int i = 0; i < SDL_NumJoysticks(); ++i)
  {
    try
    {
      devices.emplace_back(new SDL_Device(i));
    }
    catch (const std::runtime_error& e)
    {
      Service::instance().error() << "SDL input : " << e.what();
    }
  }

  return devices;
}


void
SDL_Device::init_SDL()
{
  if (s_num_sdl_devices == 0)
  {
    SDL_Init(SDL_INIT_JOYSTICK|SDL_INIT_HAPTIC|SDL_INIT_GAMECONTROLLER);
    SDL_JoystickEventState(SDL_IGNORE); // Disable polling since we don't use SDL event
    SDL_GameControllerEventState(SDL_IGNORE);
    SDL_GameControllerAddMappingsFromFile("gamecontrollerdb.txt");
  }

  s_num_sdl_devices++;
}


void
SDL_Device::fini_SDL()
{
  s_num_sdl_devices--;
}


} /* namespace Plasium */
