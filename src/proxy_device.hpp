/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PROXY_DEVICE_HPP__
#define __PROXY_DEVICE_HPP__

#include <device.hpp>

namespace Plasium
{

class Proxy_Device : public Device
{
public :
  Proxy_Device(Device* real_device, const Mapping& mapping);
  virtual ~Proxy_Device();

  virtual bool save_mapping(std::ostream&) const { return false; }
  virtual bool load_mapping(std::istream&) { return false; }

  virtual const Gamepad_State& get_state();

private :
  Device* m_real_device;
};


} /* end namespace Plasium */

#endif /* __PROXY_DEVICE_HPP__ */
