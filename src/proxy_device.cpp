/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <proxy_device.hpp>

#include <mapping.hpp>

namespace Plasium
{

Proxy_Device::Proxy_Device(Device* real_device, const Mapping& mapping)
  : m_real_device(real_device)
{
  m_mapping.reset(new Mapping(mapping));
  m_name = "Proxy_device";
  m_vendor_id  = m_mapping->get_vendor_id();
  m_product_id = m_mapping->get_product_id();
}


Proxy_Device::~Proxy_Device()
{
}


const Gamepad_State&
Proxy_Device::get_state()
{
  return m_real_device->get_state();
}


} /* end namespace Plasium */
