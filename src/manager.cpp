/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <manager.hpp>

#include <service.hpp>
#include <device.hpp>
#include <input/SDL2.hpp>
#include <input/js_api.hpp>
#include <input/evdev_device.hpp>


namespace Plasium
{
std::unique_ptr<Manager> Manager::s_instance;

Manager&
Manager::instance()
{
  if (!s_instance)
    s_instance.reset(new Manager(Service::instance().get_input_driver()));

  return *s_instance;
}


Manager::Manager(Input_Driver driver)
{
  switch (driver)
  {
  case Input_Driver::Evdev :
    Service::instance().info("Use Evdev devices");
    m_devices = EVDev_Device::get_devices();
    break;
  case Input_Driver::SDL :
    Service::instance().info("Use SDL devices");
    m_devices = SDL_Device::get_devices();
    break;
  case Input_Driver::Js :
    Service::instance().info("Use Joystick API devices");
    m_devices = Joystick_API::get_devices();
    break;
  };
}


Manager::~Manager()
{
}


std::vector<Device*>
Manager::get_devices()
{
  std::vector<Device*> list_devices;
  list_devices.reserve(m_devices.size());

  for (const auto& device : m_devices)
  {
    list_devices.emplace_back(device.get());
  }

  return list_devices;
}


} /* namespace Plasium */
