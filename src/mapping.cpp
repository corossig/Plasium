/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <mapping.hpp>

#include <gamepad.hpp>
#include <service.hpp>

#include <json/json.h>

#include <stdexcept>
#include <fstream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>


namespace Plasium
{
std::map<std::string, std::unique_ptr<Mapping>> Mapping::s_mappings;


std::string
Mapping::get_mapping_dir()
{
  struct passwd *pw = ::getpwuid(getuid());
  std::string dest(pw->pw_dir);
  dest += "/.config/plasium";

  // Create plasium config directory if not exist
  struct stat sb;
  if (stat(dest.c_str(), &sb) != 0 && mkdir(dest.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
    throw std::invalid_argument("Can't create config dir " + dest);

  // Create mapping directory if doesn't exist
  dest += "/mapping";
  if (stat(dest.c_str(), &sb) != 0 && mkdir(dest.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
    throw std::invalid_argument("Can't create mapping in config dir " + dest);

  return dest;
}

void
Mapping::parse_config()
{
  std::string mapping_dir = get_mapping_dir();

  DIR* dev_dir = opendir(mapping_dir.c_str());
  if (dev_dir)
  {
    struct dirent *ep;
    while ( (ep = readdir(dev_dir)) )
    {
      if (ep->d_type == DT_REG)
      {
        std::unique_ptr<Mapping> mapping(new Mapping(mapping_dir + "/" + ep->d_name));
        s_mappings.emplace(mapping->m_vendor_id + "_" + mapping->m_product_id, std::move(mapping));
      }
    }
    closedir(dev_dir);
  }
}



Mapping::Mapping(const std::string& filename)
{
  load_file(filename);
}


Mapping::Mapping(const std::string& vendor_id, const std::string& product_id)
  : m_vendor_id(vendor_id)
  , m_product_id(product_id)
{
  if (s_mappings.empty())
    parse_config();

  const auto& mapping = s_mappings.find(m_vendor_id + "_" + m_product_id);
  if (mapping != s_mappings.end())
  {
    *this = *(mapping->second);
    return;
  }

  // If no mapping for the specific tuple vendor_id product_id, we use default mapping
  for (int i = 0; i < static_cast<int>(Buttons_Enum::Size); ++i)
  {
    m_buttons.push_back(static_cast<Buttons_Enum>(i));
  }

  for (int i = 0; i < static_cast<int>(Axis_Enum::Size); ++i)
  {
    m_axis.push_back(static_cast<Axis_Enum>(i));
  }
}


Mapping::Mapping(const Mapping& obj)
  : m_vendor_id(obj.m_vendor_id)
  , m_product_id(obj.m_product_id)
  , m_buttons(obj.m_buttons)
  , m_axis(obj.m_axis)
  , m_hat(obj.m_hat)
{
}


Mapping::~Mapping()
{
}



void
Mapping::load_file(const std::string& filename)
{
  std::ifstream file(filename);
  if (!file.is_open())
    throw std::invalid_argument("Can't load mapping from " + filename);

  load_stream(file);
}


void
Mapping::save_file(const std::string* filename) const
{
  // If no filename, we use the standard directory
  if (filename == nullptr)
  {
    // define the final file name
    std::string dest = get_mapping_dir();
    dest += "/" + m_vendor_id + "_" + m_product_id + ".json";
    return save_file(&dest);
  }

  std::ofstream file(*filename);
  if (!file.is_open())
    throw std::invalid_argument("Can't save mapping to " + *filename);

  save_stream(file);
}


void
Mapping::load_stream(std::istream& s)
{
  Json::Value root;
  s >> root;

  Service::instance().info() << "Reading mapping for " << root["name"].asString() << std::endl;
  m_vendor_id  = root["vendor_id"].asString();
  m_product_id = root["product_id"].asString();

  Json::Value buttons = root["buttons"];
  for (const auto& key : buttons.getMemberNames())
  {
    set_button(std::stoi(key), get_button_from_name(buttons[key].asString()));
  }

  Json::Value axis = root["axis"];
  for (const auto& key : axis.getMemberNames())
  {
    std::cerr << "toto " << key << " " << axis[key].asString() << " " << static_cast<int>(get_axis_from_name(axis[key].asString())) << std::endl;
    set_axis(std::stoi(key), get_axis_from_name(axis[key].asString()));
  }

  Json::Value hat = root["hat"];
  for (const auto& key : hat.getMemberNames())
  {
    Hat_Button h;
    h.id       = std::stoi(key);
    h.positive = key.back() == '+';

    set_hat(h, get_button_from_name(hat[key].asString()));
  }
}


void
Mapping::save_stream(std::ostream& s) const
{
  Json::Value root;
  root["name"]       = "Controller";
  root["vendor_id"]  = m_vendor_id;
  root["product_id"] = m_product_id;

  Json::Value buttons;
  for (size_t i = 0; i < m_buttons.size(); ++i)
  {
    buttons[std::to_string(i)] = get_button_name(m_buttons[i]);
  }
  root["buttons"] = buttons;

  Json::Value axis;
  for (size_t i = 0; i < m_axis.size(); ++i)
  {
    axis[std::to_string(i)] = get_axis_name(m_axis[i]);
  }
  root["axis"] = axis;

  Json::Value hat;
  for (const auto& mapping : m_hat)
  {
    std::string hat_str = std::to_string(mapping.second.id);
    hat_str += (mapping.second.positive) ? '+' : '-';
    hat[hat_str] = get_button_name(mapping.first);
  }
  root["hat"] = hat;

  s << root;
}


const std::vector<Buttons_Enum>&
Mapping::get_buttons_mapping() const
{
  return m_buttons;
}


const std::vector<Axis_Enum>&
Mapping::get_axis_mapping() const
{
  return m_axis;
}


const std::map<Buttons_Enum, Hat_Button>&
Mapping::get_hat_mapping() const
{
  return m_hat;
}


void
Mapping::set_button(int i, Buttons_Enum b)
{
  // Remove old binding of button b
  for (auto& button : m_buttons)
  {
    if (button == b)
      button = Buttons_Enum::None;
  }
  m_hat.erase(b);


  // Insert null binding until vector has the right size
  while (i >= static_cast<int>(m_buttons.size()))
    m_buttons.push_back(Buttons_Enum::None);

  // Define the binding
  m_buttons[i] = b;
}


void
Mapping::set_axis(int i, Axis_Enum a)
{
  // Remove old binding of axis a
  for (auto& axis : m_axis)
  {
    if (axis == a)
      axis = Axis_Enum::None;
  }

  // Insert null binding until vector has the right size
  while (i >= static_cast<int>(m_axis.size()))
    m_axis.push_back(Axis_Enum::None);

  // Define the binding
  m_axis[i] = a;
}


void
Mapping::set_hat(Hat_Button hat, Buttons_Enum b)
{
  // Remove old binding of button b
  for (auto& button : m_buttons)
  {
    if (button == b)
      button = Buttons_Enum::None;
  }

  m_hat[b] = hat;
}


uint16_t
Mapping::get_vendor_id() const
{
  return std::stoi(m_vendor_id, nullptr, 16);
}


uint16_t
Mapping::get_product_id() const
{
  return std::stoi(m_product_id, nullptr, 16);
}

} /* namespace Plasium */
