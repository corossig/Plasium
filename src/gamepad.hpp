/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __GAMEPAD_HPP__
#define __GAMEPAD_HPP__

#include <cstdint>
#include <string>
#include <array>
#include <algorithm>


namespace Plasium
{


#undef X
#define X(TYPE, NAME, MACRO_NAME) TYPE NAME;

struct Axis
{
#include <gamepad_x_axis.inc>
};

#undef X
#define X(TYPE, NAME, MACRO_NAME, SHORT_NAME) MACRO_NAME,

enum class Buttons_Enum
{
  None = -1,
#include <gamepad_x_buttons.inc>
  Size
};


#undef X
#define X(TYPE, NAME, MACRO_NAME) MACRO_NAME,

enum class Axis_Enum
{
  None = -1,
#include <gamepad_x_axis.inc>
  Size
};

#undef X


class Buttons_Array
{
public :
  const bool& operator[](Buttons_Enum b) const { return m_array[static_cast<int>(b)]; }
  bool& operator[](Buttons_Enum b) { return m_array[static_cast<int>(b)]; }
  void clear() { std::fill(m_array.begin(), m_array.end(), false); }

  std::array<bool, static_cast<int>(Buttons_Enum::Size)> m_array;
};


class Axis_Array
{
public :
  const int16_t& operator[](Axis_Enum a) const { return m_array[static_cast<int>(a)]; }
  int16_t& operator[](Axis_Enum a) { return m_array[static_cast<int>(a)]; }
  void clear() { std::fill(m_array.begin(), m_array.end(), 0); }

  std::array<int16_t, static_cast<int>(Axis_Enum::Size)> m_array;
};


struct Gamepad_State
{
  Buttons_Array buttons;
  Axis_Array    axis;
};

extern const char* get_button_name(Buttons_Enum id);
extern const char* get_axis_name(Axis_Enum id);

extern const char* get_button_short_name(Buttons_Enum id);

extern Buttons_Enum get_button_from_name(const std::string& button_name);
extern Axis_Enum    get_axis_from_name(const std::string& axis_name);

} /* namespace Plasium */

#endif /* __GAMEPAD_HPP__ */
