/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <gamepad.hpp>

#include <map>

namespace Plasium
{

#undef X
#define X(TYPE, NAME, MACRO_NAME, SHORT_NAME) case Buttons_Enum::MACRO_NAME :      \
                                                return #MACRO_NAME;

const char*
get_button_name(Buttons_Enum id)
{
  switch (id)
  {
  case Buttons_Enum::None :
    return "None";
#include <gamepad_x_buttons.inc>
  case Buttons_Enum::Size :
    return "None";
  }

  return nullptr;
}

#undef X
#define X(TYPE, NAME, MACRO_NAME) case Axis_Enum::MACRO_NAME : \
                                    return #MACRO_NAME;

const char*
get_axis_name(Axis_Enum id)
{
  switch (id)
  {
  case Axis_Enum::None :
    return "None";
#include <gamepad_x_axis.inc>
  case Axis_Enum::Size :
    return "None";
  }

  return nullptr;
}


#undef X
#define X(TYPE, NAME, MACRO_NAME, SHORT_NAME) {#MACRO_NAME, Buttons_Enum::MACRO_NAME},

Buttons_Enum
get_button_from_name(const std::string& button_name)
{
  static std::map<std::string, Buttons_Enum> map_buttons {
#include <gamepad_x_buttons.inc>
  };

  return map_buttons[button_name];
}


#undef X
#define X(TYPE, NAME, MACRO_NAME) {#MACRO_NAME, Axis_Enum::MACRO_NAME},
Axis_Enum
get_axis_from_name(const std::string& axis_name)
{
  static std::map<std::string, Axis_Enum> map_axis {
#include <gamepad_x_axis.inc>
  };

  return map_axis[axis_name];
}


#undef X
#define X(TYPE, NAME, MACRO_NAME, SHORT_NAME) case Buttons_Enum::MACRO_NAME :      \
                                                return #SHORT_NAME;

const char*
get_button_short_name(Buttons_Enum id)
{
  switch (id)
  {
  case Buttons_Enum::None :
    return "None";
#include <gamepad_x_buttons.inc>
  case Buttons_Enum::Size :
    return "None";
  }

  return nullptr;
}


} /* namespace Plasium */
