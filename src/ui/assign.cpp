/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include "ui_assign.h"
#include <ui/assign.hpp>

#include <QDebug>
#include <sstream>

#include <ui/device_svg.hpp>
#include <device.hpp>

namespace Plasium
{

Assign_Thread::Assign_Thread(Device* device)
  : m_device(device)
  , m_break(false)
  , m_continue(false)
  , m_count(0)
{
}

Assign_Thread::~Assign_Thread()
{
}


void
Assign_Thread::previous_asking()
{
  std::unique_lock<std::mutex> lock(m_mutex);
  if (m_count > 1)
  {
    m_count -= 2;
    m_continue = true;
    m_cv.notify_one();
  }
}


void
Assign_Thread::start_asking()
{
  std::unique_lock<std::mutex> lock(m_mutex);
  m_count++;
  m_cv.notify_one();
}


void
Assign_Thread::next_asking()
{
  std::unique_lock<std::mutex> lock(m_mutex);
  m_continue = true;
}


void
Assign_Thread::stop_asking()
{
  std::unique_lock<std::mutex> lock(m_mutex);
  m_continue = true;
  m_break    = true;
}


void
Assign_Thread::cancel()
{
  std::unique_lock<std::mutex> lock(m_mutex);
  m_continue = true;
  m_break    = true;

  std::stringstream ss(m_old_mapping);
  m_device->load_mapping(ss);
}


void
Assign_Thread::run()
{
  std::stringstream ss;
  m_device->save_mapping(ss);
  m_old_mapping = ss.str();

  {
    std::unique_lock<std::mutex> lock(m_mutex);

    while (m_count < 17 && !m_break)
    {
      Buttons_Enum b = static_cast<Buttons_Enum>(m_count);
      emit ask_button(m_count);

      // Start asking only when the message is setup
      m_cv.wait(lock, [&](){return m_count < 17;});
      m_continue = false;

      lock.unlock();
      m_device->assign_buttons(b, &m_continue);
      lock.lock();
    }
  }

  if (!m_break)
    emit finished();
}




Assign::Assign(QWidget *parent, Device* device)
  : QDialog(parent)
  , m_ui(new Ui::assign)
  , m_svg(new Device_SVG(this))
  , m_device(device)
  , m_thread(device)
{
  m_ui->setupUi(this);

  // Add device picture
  m_svg->set_level(5);
  m_ui->layout->addWidget(m_svg->get_widget());



  connect(&m_thread, SIGNAL( ask_button(int) ),
          this,        SLOT( thread_ask_button(int) ));

  connect(&m_thread, SIGNAL( finished() ),
          this,        SLOT( accept() ));

  connect(m_ui->b_next, SIGNAL( clicked() ),
          this,           SLOT( b_next_handler() ));

  connect(m_ui->b_previous, SIGNAL( clicked() ),
          this,               SLOT( b_previous_handler() ));

  connect(m_ui->b_cancel, SIGNAL( clicked() ),
          this,             SLOT( b_cancel_handler() ));

  connect(m_ui->b_validate, SIGNAL( clicked() ),
          this,               SLOT( b_validate_handler() ));

  m_thread.start();
}



Assign::~Assign()
{
  if (!m_thread.wait(1000))
    m_thread.terminate();
}


void
Assign::thread_ask_button(int b)
{
  QString msg("Please press twice %1 button");
  m_ui->l_ask->setText(msg.arg(get_button_name((Buttons_Enum)b)));

  // Set view port center on button
  m_svg->zoom_on((Buttons_Enum)b);

  m_thread.start_asking();
}


void
Assign::thread_ask_axis(Axis_Enum b)
{
  QString msg("Please move %1 axis");
  m_ui->l_ask->setText(msg.arg(get_axis_name(b)));
  m_thread.start_asking();
}


void
Assign::b_next_handler()
{
  m_thread.next_asking();
}


void
Assign::b_previous_handler()
{
  m_thread.previous_asking();
}


void
Assign::b_cancel_handler()
{
  m_thread.stop_asking();
  reject();
}


void
Assign::b_validate_handler()
{
  m_thread.stop_asking();
  accept();
}


void
Assign::closeEvent(QCloseEvent *)
{
  m_thread.stop_asking();
  reject();
}


} /* namespace Plasium */

#include "moc_assign.cpp"
