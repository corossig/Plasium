/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DEVICE_CFG_HPP__
#define __DEVICE_CFG_HPP__

#include <QMainWindow>
#include <QTimer>
#include <QGraphicsScene>
#include <QMovie>

#include <vector>
#include <memory>

namespace Ui
{

class device_cfg;

} /* namespace Ui */


namespace Plasium
{
class Device;
class Device_SVG;
class Axis_1D;

class Device_Cfg : public QMainWindow
{
  Q_OBJECT

public:
  explicit Device_Cfg(QWidget *parent = nullptr);
  ~Device_Cfg() override;

public slots:
  void type_update_handler(int);
  void assign_handler();
  void save();
  void update_devices();
  void movie_ok_frame(int frame);

private:
  Ui::device_cfg *ui;
  std::unique_ptr<Device_SVG> m_device_svg;

  QTimer m_device_timer; /* poll status of device */
  QMovie m_ok_movie; /* Animated ok checkbox */

  QGraphicsScene m_left_axis_scene;
  QGraphicsScene m_right_axis_scene;

  std::vector<Device*> m_devices;
  Device* m_current_device;

  std::vector<std::unique_ptr<Axis_1D> > m_axis_1d;
};


} /* namespace Plasium */

#endif /* __DEVICE_CFG_HPP__ */
