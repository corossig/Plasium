/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include "ui_axis_1d.h"
#include <ui/axis_1d.hpp>

#include <QString>

namespace Plasium
{

Axis_1D::Axis_1D(QWidget *parent)
  : QWidget(parent)
  , ui(new Ui::axis_1d)
{
  ui->setupUi(this);

  ui->b_value->setRange(-32768, 32767);
}


Axis_1D::~Axis_1D()
{
}


void
Axis_1D::update_value(int value)
{
  ui->b_value->setValue(value);
}


void
Axis_1D::set_label(const std::string &value)
{
  ui->l_axis->setText(value.c_str());
}

} /* namespace Plasium */

#include "moc_axis_1d.cpp"
