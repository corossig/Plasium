/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __ASPECT_RATIO_WIDGET_HPP__
#define __ASPECT_RATIO_WIDGET_HPP__

#include <QWidget>
#include <memory>

class QBoxLayout;

namespace Plasium
{

class AspectRatioWidget : public QWidget
{
public:
  AspectRatioWidget(QWidget *widget, float width, float height, QWidget *parent=nullptr);
  ~AspectRatioWidget() override;

  void update_size(float new_width, float new_height);

  void resizeEvent(QResizeEvent *event);

private:
  std::unique_ptr<QBoxLayout> m_layout;
  float m_width;
  float m_height;
};

} /* namespace Plasium */

#endif /* __ASPECT_RATIO_WIDGET_HPP__ */
