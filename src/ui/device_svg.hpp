/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DEVICE_SVG_HPP__
#define __DEVICE_SVG_HPP__

#include <QByteArray>
#include <QSize>

#include <memory>

class QWidget;
class QSvgWidget;
class QDomDocument;
class QDomElement;
class QString;

namespace Plasium
{
class AspectRatioWidget;

struct Gamepad_State;
enum class Buttons_Enum;


class Device_SVG
{
public :
  Device_SVG(QWidget *parent = nullptr);
  ~Device_SVG();

  void set_level(int level);
  QWidget* get_widget();

  void zoom_on(Buttons_Enum b);
  void define_highlight(const Gamepad_State& state);

private :
  QByteArray get_svg(int level);

  void color_button(const QString& id);
  static QDomElement find_by_id(const QDomElement& elem, const QString& id);

private :
  std::unique_ptr<QSvgWidget>        m_svg_widget;
  std::unique_ptr<AspectRatioWidget> m_aspect_ratio_widget;
  QSize m_image_size;

  QByteArray m_initial_data;
  std::unique_ptr<QDomDocument> m_dom;
};


} /* namespace Plasium */

#endif /* __DEVICE_SVG_HPP__ */
