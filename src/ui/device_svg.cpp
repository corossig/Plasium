/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <ui/device_svg.hpp>

#include <gamepad.hpp>
#include <ui/aspect_ratio_widget.hpp>

#include <QtXml>
#include <QSvgWidget>
#include <QSvgRenderer>
#include <QFile>

namespace Plasium
{

Device_SVG::Device_SVG(QWidget *parent)
  : m_svg_widget(new QSvgWidget(parent))
  , m_aspect_ratio_widget(new AspectRatioWidget(m_svg_widget.get(), 1, 1, parent))
  , m_dom(new QDomDocument(""))
{
}


Device_SVG::~Device_SVG()
{
  // The destructor order is important
  m_svg_widget.reset();
  m_aspect_ratio_widget.reset();
}


QWidget*
Device_SVG::get_widget()
{
  return m_aspect_ratio_widget.get();
}


QByteArray
Device_SVG::get_svg(int level)
{
  QFile svg_file(":/assets/level_" + QString::number(level) + ".svg");
  svg_file.open(QIODevice::ReadOnly);

  m_initial_data = svg_file.readAll();
  m_dom->setContent(m_initial_data);

  QImage gamepad_image(":/assets/level_" + QString::number(level) + ".svg");
  m_image_size = gamepad_image.size();

  return m_dom->toByteArray();
}


void
Device_SVG::set_level(int level)
{
  m_svg_widget->load(get_svg(level));

  // Find good ratio
  m_aspect_ratio_widget->update_size(m_image_size.width(), m_image_size.height());
  m_aspect_ratio_widget->adjustSize();
}


void
Device_SVG::zoom_on(Buttons_Enum b)
{
  auto renderer = m_svg_widget->renderer();
  QString b_id  = get_button_short_name(b);
  auto bounds   = renderer->boundsOnElement(b_id);
  auto matrix   = renderer->matrixForElement(b_id);

  QPointF center = matrix.mapRect(bounds).center();

  QRectF view_box(0, 0, 0.20*m_image_size.width(), 0.20*m_image_size.height());
  view_box.moveCenter(center);
  renderer->setViewBox(view_box);

  m_svg_widget->repaint();
}


void
Device_SVG::define_highlight(const Gamepad_State& device_state)
{
  m_dom->setContent(m_initial_data);

  for (int i = 0; i < static_cast<int>(Buttons_Enum::Size); ++i)
  {
    Buttons_Enum b = static_cast<Buttons_Enum>(i);
    if (device_state.buttons[b])
      color_button(get_button_short_name(b));
  }

  // Update image
  m_svg_widget->load(m_dom->toByteArray());
}


void
Device_SVG::color_button(const QString& id)
{
  QDomElement button_elem = find_by_id(m_dom->documentElement(), id);
  if (!button_elem.isNull())
  {
    QString style = button_elem.attributes().namedItem("style").nodeValue();
    int index_of_fill = style.indexOf("fill:");
    style.replace(index_of_fill, 12, "fill:#00ff00");
    button_elem.setAttribute("style", style);
  }
}


QDomElement
Device_SVG::find_by_id(const QDomElement& elem, const QString& id)
{
  // Is the current node the on we seek
  const auto& id_attr = elem.attributes().namedItem("id");
  if( !id_attr.isNull() && id_attr.nodeValue() == id)
  {
    return elem;
  }

  QDomElement child = elem.firstChildElement();
  while( !child.isNull() )
  {
    QDomElement dom_elem = find_by_id(child, id);
    if (!dom_elem.isNull())
      return dom_elem;

    child = child.nextSiblingElement();
  }

  return child; //< null element
}

} /* namespace Plasium */
