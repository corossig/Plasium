/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <src/ui/aspect_ratio_widget.hpp>

#include <QBoxLayout>
#include <QResizeEvent>

namespace Plasium
{

AspectRatioWidget::AspectRatioWidget(QWidget *widget, float width, float height, QWidget *parent)
  : QWidget(parent)
  , m_layout(new QBoxLayout(QBoxLayout::LeftToRight, this))
  , m_width(width)
  , m_height(height)
{
  // add spacer, then the widget, then another spacer
  m_layout->addItem(new QSpacerItem(0, 0));
  m_layout->addWidget(widget);
  m_layout->addItem(new QSpacerItem(0, 0));
}


AspectRatioWidget::~AspectRatioWidget()
{
}


void
AspectRatioWidget::update_size(float new_width, float new_height)
{
  m_width  = new_width;
  m_height = new_height;
}


void
AspectRatioWidget::resizeEvent(QResizeEvent *event)
{
  float thisAspectRatio = (float)event->size().width() / event->size().height();
  int widgetStretch, outerStretch;

  if (thisAspectRatio > (m_width/m_height)) // too wide
  {
    m_layout->setDirection(QBoxLayout::LeftToRight);
    widgetStretch = height() * (m_width/m_height); // i.e., my width
    outerStretch = (width() - widgetStretch) / 2 + 0.5;
  }
  else // too tall
  {
    m_layout->setDirection(QBoxLayout::TopToBottom);
    widgetStretch = width() * (m_height/m_width); // i.e., my height
    outerStretch = (height() - widgetStretch) / 2 + 0.5;
  }

  m_layout->setStretch(0, outerStretch);
  m_layout->setStretch(1, widgetStretch);
  m_layout->setStretch(2, outerStretch);
}

} /* namespace Plasium */
