/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __UI_ASSIGN_HPP__
#define __UI_ASSIGN_HPP__

#include <QDialog>
#include <QTimer>
#include <QThread>
#include <QSvgWidget>

#include <condition_variable>
#include <mutex>

class QAbstractButton;

namespace Ui
{

class assign;

} /* namespace Ui */


namespace Plasium
{
class Device;
class Device_SVG;
enum class Buttons_Enum;
enum class Axis_Enum;

class Assign_Thread : public QThread
{
  Q_OBJECT

public:
  explicit Assign_Thread(Device* device);
  ~Assign_Thread() override;

  void start_asking();
  void next_asking();
  void previous_asking();
  void stop_asking();
  void cancel();

signals:
  void ask_button(int b);
  void ask_axis(Axis_Enum b);
  void finished();

private:
  void run() override;

private:
  Device*     m_device;
  std::string m_old_mapping;

  bool m_break;
  bool m_continue;

  int                     m_count;
  std::mutex              m_mutex;
  std::condition_variable m_cv;
};

class Assign : public QDialog
{
  Q_OBJECT

public:
  explicit Assign(QWidget *parent, Device* device);
  ~Assign() override;

public slots:
  void thread_ask_button(int b);
  void thread_ask_axis(Axis_Enum b);

  void b_next_handler();
  void b_previous_handler();
  void b_cancel_handler();
  void b_validate_handler();

private :
  void closeEvent(QCloseEvent *event) override;

private :
  std::unique_ptr<Ui::assign> m_ui;

  std::unique_ptr<Device_SVG> m_svg;

  Device* m_device;
  Assign_Thread m_thread;      /* Thread wating for blocking input */
};


} /* namespace Plasium */

#endif /* __DEVICE_CFG_HPP__ */
