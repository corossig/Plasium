/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __AXIS_1D_HPP__
#define __AXIS_1D_HPP__

#include <QWidget>

#include <string>
#include <memory>

namespace Ui
{

class axis_1d;

} /* namespace Ui */


namespace Plasium
{
class Axis_1D : public QWidget
{
  Q_OBJECT

public:
  explicit Axis_1D(QWidget *parent = nullptr);
  ~Axis_1D() override;

public slots:
  void update_value(int);
  void set_label(const std::string &);

private:
  std::unique_ptr<Ui::axis_1d> ui;
};

} /* namespace Plasium */

#endif /* __AXIS_1D_HPP__ */
