/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include "ui_device_cfg.h"
#include <ui/device_cfg.hpp>

#include <ui/device_svg.hpp>
#include <ui/assign.hpp>
#include <ui/axis_1d.hpp>

#include <service.hpp>
#include <manager.hpp>
#include <device.hpp>

#include <QGraphicsPixmapItem>
#include <QSvgRenderer>
#include <QDebug>
#include <QGraphicsColorizeEffect>
#include <QPropertyAnimation>

namespace Plasium
{

Device_Cfg::Device_Cfg(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::device_cfg)
  , m_device_svg(new Device_SVG(this))
  , m_device_timer(this)
  , m_ok_movie(this)
  , m_current_device(nullptr)
{
  ui->setupUi(this);
  setWindowTitle("Plasium_Qt");
  setWindowIcon(QIcon(":/assets/logo_128.png"));

  ui->left_layout->addWidget(m_device_svg->get_widget());

  m_devices = Manager::instance().get_devices();
  if (!m_devices.empty())
    m_current_device = m_devices[0];

  ui->cb_type->addItem("Level 1", 1);
  ui->cb_type->addItem("Level 2", 2);
  ui->cb_type->addItem("Level 3", 3);
  ui->cb_type->addItem("Level 4", 4);
  ui->cb_type->addItem("Level 5", 5);

  connect(ui->cb_type, SIGNAL( activated(int) ),
          this,        SLOT( type_update_handler(int) ));

  connect(ui->B_assign, SIGNAL( clicked() ),
          this,         SLOT( assign_handler() ));

  connect(ui->B_save, SIGNAL( clicked() ),
          this,         SLOT( save() ));

  ui->cb_emulate->addItem("None", -1);

  // Add L2 and R2 axis
  m_axis_1d.emplace_back(new Axis_1D());
  m_axis_1d.emplace_back(new Axis_1D());
  m_axis_1d[0]->set_label("L2");
  m_axis_1d[1]->set_label("R2");
  ui->axisLayout->addWidget(m_axis_1d[0].get());
  ui->axisLayout->addWidget(m_axis_1d[1].get());

  m_left_axis_scene.setSceneRect(0, 0, 64, 64);
  m_right_axis_scene.setSceneRect(0, 0, 64, 64);
  ui->gv_leftaxis->setScene(&m_left_axis_scene);
  ui->gv_rightaxis->setScene(&m_right_axis_scene);

  // Ask gamepad every 20ms (50Hz)
  m_device_timer.setInterval(20);
  connect(&m_device_timer, SIGNAL( timeout() ),
          this,            SLOT( update_devices() ));
  m_device_timer.start();

  if (m_devices.size() > 0)
  {
    ui->cb_type->setCurrentIndex(m_devices.front()->max_level_buttons()-1);
    type_update_handler( ui->cb_type->currentIndex() );
  }


  m_ok_movie.setFileName(":/assets/ok.gif");
  m_ok_movie.setSpeed(600);
  connect(&m_ok_movie, SIGNAL( frameChanged(int) ),
          this,          SLOT( movie_ok_frame(int) ));

#if 0
  QGraphicsColorizeEffect* a = new QGraphicsColorizeEffect(this);
  a->setColor(QColor("blue"));
  m_svg_widget.setGraphicsEffect(a);
#endif
}

Device_Cfg::~Device_Cfg()
{
  delete ui;
}


void
Device_Cfg::type_update_handler(int)
{
  m_device_svg->set_level(ui->cb_type->currentData().toInt());
}


void
Device_Cfg::assign_handler()
{
  if (m_current_device)
  {
    Assign a(this, m_current_device);
    a.exec();
  }
}


void
Device_Cfg::save()
{
  if (m_current_device)
  {
    m_current_device->save();
    m_ok_movie.start();
  }
}


void
Device_Cfg::movie_ok_frame(int frame)
{
  if (frame < m_ok_movie.frameCount()-1)
  {
    ui->B_save->setIcon(QIcon(m_ok_movie.currentPixmap()));
  }
  else
  {
    ui->B_save->setIcon(QIcon());
    m_ok_movie.stop();
  }
}


void
Device_Cfg::update_devices()
{
  ui->PTE_status->clear();

  if (m_current_device)
  {
    auto& device_state = m_current_device->get_state();

    // Update central image
    m_device_svg->define_highlight(device_state);

    ui->PTE_status->appendPlainText("Found " + QString::number(m_devices.size()) + " devices :");
    ui->PTE_status->appendPlainText(QString(" - ") + m_current_device->get_name().c_str());

    int left_x_pos  = 32*(1+(float(device_state.axis[Axis_Enum::LEFT_X])/float(std::numeric_limits<int16_t>::max())));
    int left_y_pos  = 32*(1+(float(device_state.axis[Axis_Enum::LEFT_Y])/float(std::numeric_limits<int16_t>::max())));
    int right_x_pos = 32*(1+(float(device_state.axis[Axis_Enum::RIGHT_X])/float(std::numeric_limits<int16_t>::max())));
    int right_y_pos = 32*(1+(float(device_state.axis[Axis_Enum::RIGHT_Y])/float(std::numeric_limits<int16_t>::max())));

    m_left_axis_scene.clear();
    m_right_axis_scene.clear();

    m_left_axis_scene.addLine(left_x_pos-10, left_y_pos, left_x_pos+10, left_y_pos, QPen(QBrush(Qt::red), 2));
    m_left_axis_scene.addLine(left_x_pos, left_y_pos-10, left_x_pos, left_y_pos+10, QPen(QBrush(Qt::red), 2));
    m_right_axis_scene.addLine(right_x_pos-10, right_y_pos, right_x_pos+10, right_y_pos, QPen(QBrush(Qt::blue), 2));
    m_right_axis_scene.addLine(right_x_pos, right_y_pos-10, right_x_pos, right_y_pos+10, QPen(QBrush(Qt::blue), 2));

    ui->gv_leftaxis->fitInView(m_left_axis_scene.sceneRect(), Qt::KeepAspectRatio);
    ui->gv_rightaxis->fitInView(m_right_axis_scene.sceneRect(), Qt::KeepAspectRatio);

    ui->gv_leftaxis->show();
    ui->gv_rightaxis->show();

    for (int i = 0; i < static_cast<int>(Buttons_Enum::Size); ++i)
    {
      Buttons_Enum b = static_cast<Buttons_Enum>(i);
      if (device_state.buttons[b])
      {
        Service::instance().info() << "  - " << get_button_name(b) << std::endl;
        ui->PTE_status->appendPlainText(QString("  - ") + get_button_name(b));
      }
    }

    m_axis_1d[0]->update_value(device_state.axis[Axis_Enum::L2]);
    m_axis_1d[1]->update_value(device_state.axis[Axis_Enum::R2]);
  }
}



} /* namespace Plasium */

#include "moc_device_cfg.cpp"
