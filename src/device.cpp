/*
 *  Copyright (C) 2016 Corentin Rossignon
 *
 *  This file is part of Plasium.
 *
 *  Plasium is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Plasium is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Plasium. If not, see <http://www.gnu.org/licenses/>.
 */
#include <device.hpp>

#include <mapping.hpp>

#include <cstring>
#include <sstream>
#include <iomanip>

namespace Plasium
{

Device::Device()
  : m_name("Unknown")
  , m_vendor_id(0)
  , m_product_id(0)
  , m_level_buttons(5)
  , m_level_axis(5)
{
  std::memset(&m_state, 0, sizeof(Gamepad_State));
}


Device::~Device()
{
}


const std::string&
Device::get_name() const
{
  return m_name;
}


int
Device::max_level_buttons() const
{
  return m_level_axis;
}


int
Device::max_level_axis() const
{
  return m_level_buttons;
}


void
Device::save() const
{
  m_mapping->save_file();
}


const std::vector<bool>&
Device::get_raw_buttons_state()
{
  get_state();
  const auto& mapping = m_mapping->get_buttons_mapping();
  m_raw_buttons_state.resize(mapping.size(), false);

  for (size_t i = 0; i < mapping.size(); ++i)
  {
    if (mapping[i] == Buttons_Enum::None)
      m_raw_buttons_state[i] = false;
    else
      m_raw_buttons_state[i] = m_state.buttons[mapping[i]];
  }

  return m_raw_buttons_state;
}


std::string
Device::ids_to_string(uint16_t id)
{
  std::stringstream ss;
  ss << std::setfill ('0') << std::setw(4) << std::hex << id;
  return ss.str();
}

} /* namespace Plasium */
