X(int16_t, left_x,  LEFT_X)  // left-right joystick on the left side of the gamepad
X(int16_t, left_y,  LEFT_Y)  // top-bottom joystick on the left side of the gamepad
X(int16_t, right_x, RIGHT_X) // left-right joystick on the right side of the gamepad
X(int16_t, right_y, RIGHT_Y) // top-bottom joystick on the right side of the gamepad

X(int16_t, L2, L2) // axis on the left trigger
X(int16_t, R2, R2) // axis on the right trigger
