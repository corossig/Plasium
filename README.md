# Plasium

This library provides a binary interface for using gamepad in a game.
I want to address many problems related to gamepad :

* Non-uniform key mapping
* Lose of calibration
* Variability of support

To solve these problems, Plasium will provide :

* Coherent interface (C/C++) for new games
* LD_PRELOAD and/or Ptrace wrapper (for supporting old games)
* Wrapper for wine (inspirated by koku-xinput-wine)
* Device mapping database (first version will be imported from SDL)
* GUI for making binding and calibrattion easier
